#include "TerGenSimplex.h"
#include "WorldView.h"
#include "Vector3.h"
#include "lib/FastNoiseSIMD.h"
#include "VoxelEngine.h"
#include "stdlib.h"
#include <omp.h>
static float** noise;
static float *testSet,*testSet2;
#define NOISE_SET_SIZE omp_get_num_procs()

static FastNoiseSIMD *fastNoise;
void InitTGSNoiseSet(uint32_t seed)
{
    noise = (float**)malloc(sizeof(float*) * NOISE_SET_SIZE);
    for(int i = 0; i < NOISE_SET_SIZE; i++)
        noise[i] = FastNoiseSIMD::GetEmptySet(CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE);
    testSet = FastNoiseSIMD::GetEmptySet(CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE);
    testSet2 = FastNoiseSIMD::GetEmptySet(CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE);
    fastNoise = FastNoiseSIMD::NewFastNoiseSIMD(seed);
    fastNoise->SetNoiseType(FastNoiseSIMD::NoiseType::SimplexFractal);
}

typedef union {
        float* noise;
        // f32x8* vec;
        f32x16* vec;
} NoiseVec_t;

void DestroyTGS()
{   
    for(int i = 0; i < NOISE_SET_SIZE; i++)
        FastNoiseSIMD::FreeNoiseSet(noise[i]);
    FastNoiseSIMD::FreeNoiseSet(testSet);
    FastNoiseSIMD::FreeNoiseSet(testSet2);
    free(noise);
    delete(fastNoise);

}

void generateTGS(WorldView_t* view,Vector3_32_t* id){

    NoiseVec_t ourNoise = {
        .noise = noise[omp_get_thread_num()]
    };
    fastNoise->FillNoiseSet(ourNoise.noise,id->coords.x * CHUNK_SIZE,id->coords.y * CHUNK_SIZE,id->coords.z * CHUNK_SIZE,CHUNK_SIZE,CHUNK_SIZE,CHUNK_SIZE);
    Chunk_t* chunk = getChunk(view,id);
    float worldY = (id->coords.y * CHUNK_SIZE);
    for(int8_t x = 0; x < CHUNK_SIZE; x++)
    for(int8_t y = 0; y < CHUNK_SIZE; y++)
    // for(uint8_t z = 0; z < CHUNK_SIZE/8; z++)
    // for(uint8_t z = 0; z < CHUNK_SIZE; z++)
    {
        // uint16_t index = (x << 8) + (y << 4) + z;
        // uint16_t index = (x  << 5) + (y  << 1)  + z;
        uint8_t index = (x << 4) + y;

        i32x16 val = ((ourNoise.vec[index] * 160.0f) < (worldY + y));

        u16x16 uval = __builtin_convertvector(val,u16x16);

        chunk->voxels.voxvec[x][y] = uval;
    }
    #ifdef TERGEN_DEBUG
    
    Vector3_32_t testID;
    testID.coords.x = id->coords.x - 1;
    testID.coords.y = id->coords.y;
    testID.coords.z = id->coords.z;
    
    Chunk_t* testChunk = getChunk(view,&testID);
    uint8_t diffCount = 0;
    
    if(vEquals(&testID,&testChunk->id) && testChunk->loaded)
    for(int z = 0; z < CHUNK_SIZE; z++)
    {
        if(chunk->voxels.voxels[0][0][z].type == 0xFFFF)
        if(testChunk->voxels.voxels[0xF][0][z].type != chunk->voxels.voxels[0][0][z].type && testChunk->voxels.voxels[0xE][0][z].type != chunk->voxels.voxels[0][0][z].type && chunk->voxels.voxels[1][0][z].type != chunk->voxels.voxels[0][0][z].type)
        {
            diffCount++;
        }
    }
    #pragma omp critical
    if(diffCount > 8)
    {
        printf("found weird chunk %i,%i,%i test chunk coords: %i,%i,%i printing Z15 (prev chunk) Z0 and Z1 noise and types\n",id->coords.x,id->coords.y,id->coords.z,testID.coords.x,testID.coords.y,testID.coords.z);

        // fastNoise->FillNoiseSet(testSet,id->coords.x * CHUNK_SIZE,id->coords.y * CHUNK_SIZE,id->coords.z * CHUNK_SIZE,CHUNK_SIZE,CHUNK_SIZE,CHUNK_SIZE);

        // for(int i = 0; i < 4096; i++)
        // {
        //     if(testSet[i] != ourNoise.noise[i])
        //     {
        //         printf("this is impossible, I told you it was impossible\n");
        //     }
        // }


        fastNoise->FillNoiseSet(testSet,(id->coords.x - 1) * CHUNK_SIZE,id->coords.y * CHUNK_SIZE,id->coords.z * CHUNK_SIZE,CHUNK_SIZE,CHUNK_SIZE,CHUNK_SIZE);
        fastNoise->FillNoiseSet(testSet2,id->coords.x *CHUNK_SIZE,id->coords.y * CHUNK_SIZE,id->coords.z * CHUNK_SIZE,CHUNK_SIZE,CHUNK_SIZE,CHUNK_SIZE);
        for(int i = 0; i < 4096; i++)
        {
            if(testSet2[i] != ourNoise[i])
            {
                printf("Discrepency found in noise generation\n");
                break;
            }
        }
        for(int z = 0; z < CHUNK_SIZE; z++)
        {
            printf("%f %f %f %f %x %x %x %x\n",testSet[3584 + z],testSet[3840 + z],ourNoise[z],ourNoise[256 + z],testChunk->voxels.voxels[0xE][0][z],testChunk->voxels.voxels[0xF][0][z], chunk->voxels.voxels[0][0][z].type,chunk->voxels.voxels[1][0][z].type);
        }
        
        assert(0);
    }
    
    #endif

}

#ifndef BMP_H
#define BMP_H
#include <stdint.h>
#pragma pack(push, 1)
typedef struct {
    uint16_t headerField;
    uint32_t size;
    uint16_t reserved1;
    uint16_t reserved2;
    uint32_t pixelOffset;
} BitmapHeader_t;

typedef struct {
    uint32_t dibSize;
    int32_t width;
    int32_t height;
    uint16_t color_planes;
    uint16_t bpp;
    uint32_t compression;
    uint32_t imageSize;
    int32_t horizantal_res;
    int32_t vertical_res;
    uint32_t colorsUsed;
    uint32_t importantColorsUsed;
} DIBHeader_t;

#pragma pack(pop)

#define BMP_SUCCESS 0
#define BMP_ERROR_NOT_BMP_FILE 1
#define BMP_ERROR_CORRUPT 2
#define BMP_ERROR_HANK 3
#define BMP_INVALID_HEADER 4
#define BMP_INVALID_PIXELS 5

#define BI_RGB 0
#define BI_RLE8 1
#define BI_RLE4 2
#define BI_BITFIELDS 3
#define BI_JPEG 4
#define BI_PNG 5
#define BI_ALPHABITFIELDS 6
#define BI_CMYK 11
#define BI_CMYKRLE8 12
#define BI_CMYKRLE4 13

#ifdef __cplusplus
extern "C"{
#endif
    uint8_t bmp_loadFile(const char *fileName, BitmapHeader_t *header, DIBHeader_t *dibHeader, char **pixels);
#ifdef __cplusplus
}
#endif

#endif
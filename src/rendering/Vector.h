#ifndef VECTOR_H
#define VECTOR_H

// #define VECX 0
// #define VECY 1
// #define VECZ 2

// #define VERTEX_VECTOR_TYPE(vectype, type, name, size) \
// typedef struct { \
//     union {\
//    vectype vec[size];  \
//    struct {\
//     type pos[size];\
//    } ;\
//     }; \
// } Vec## name##_##size##_t;  

typedef float f32x2 __attribute__ ((vector_size(8)));
typedef float f32x3 __attribute__ ((vector_size(16)));
typedef float f32x4 __attribute__ ((vector_size(16)));
typedef float mat4  __attribute__ ((vector_size(16*4)));

// VERTEX_VECTOR_TYPE(f32x2,float,f32,2)

#define _vecx 0
#define _vecy 1
#define _vecz 2

#define VECX(vec) vec[_vecx]
#define VECY(vec) vec[_vecy]
#define VECZ(vec) vec[_vecz]

#define VECR(vec) VECX(vec)
#define VECG(vec) VECY(vec)
#define VECB(vec) VECZ(vec)


#endif
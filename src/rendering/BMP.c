#include "BMP.h"
#include <stdio.h>
#include <stdlib.h>
uint8_t bmp_loadFile(const char *fileName, BitmapHeader_t *header, DIBHeader_t *dibHeader, char **pixels){
    FILE *bmpFile;
    bmpFile = fopen(fileName,"rb");
    if(fread(header,1,sizeof(BitmapHeader_t),bmpFile) != sizeof(BitmapHeader_t)) return BMP_INVALID_HEADER;
    if(header->headerField != 0x4D42){
        printf("header field value not 0x4D42 : %x\n",header->headerField);
        return BMP_ERROR_NOT_BMP_FILE;
    } 
    dibHeader->compression = 0; //to ensure we don't read garbage later
    uint32_t dibSize = 0;
    if(fread(&dibSize,1,sizeof(uint32_t),bmpFile) != sizeof(uint32_t)) return BMP_ERROR_CORRUPT;

    dibHeader->dibSize = dibSize;
    size_t readSize = fread(&dibHeader->width,1,dibSize - sizeof(uint32_t),bmpFile);
    if(readSize != dibSize - sizeof(uint32_t)){
        printf("dib read invalid, size: %zu\n",readSize);
        return BMP_INVALID_HEADER;
    } 
    printf("dib header values: \n");
    printf("w: %x\n",dibHeader->width);
    printf("h: %x\n",dibHeader->height);
    printf("colorplanes: %x\n",dibHeader->color_planes);
    printf("bpp: %x\n",dibHeader->bpp);
    printf("compression: %x\n",dibHeader->compression);
    printf("bmp size: %x\n",dibHeader->imageSize);

    if(dibHeader->compression == BI_BITFIELDS || dibHeader->compression == BI_ALPHABITFIELDS)
    {
        //could have 12-16 bytes here to use for whatever in the fuck
    }
    if(dibHeader->compression == BI_JPEG)
    {
        printf("Do I look like I know what a JPEG is?\n");
        return BMP_ERROR_HANK;
    }
    
    fseek(bmpFile,header->pixelOffset,SEEK_SET);
    *pixels = malloc(((abs(dibHeader->width * dibHeader->bpp + 31)/32)*4) * abs(dibHeader->height));
    readSize = fread(*pixels,(abs(dibHeader->width * dibHeader->bpp + 31)/32)*4,abs(dibHeader->height),bmpFile);
    if(readSize != abs(dibHeader->height)){
        printf("invalid read size for pixels: %zu expected: %i\n",readSize, abs(dibHeader->height));
        return BMP_INVALID_PIXELS;
    }
    fclose(bmpFile);
    return BMP_SUCCESS;


}
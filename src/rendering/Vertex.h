#ifndef VERTEX_H
#define VERTEX_H
#include "Vector.h"
#include <vulkan/vulkan.h>

typedef struct {
    f32x2 pos;
    f32x3 color;
    f32x2 texCoord;
} Vertex2_t;

typedef struct {
    f32x3 pos;
    f32x3 color;
    f32x2 texCoord;
} Vertex3_t;

VkVertexInputBindingDescription vert2_getBindingDescription(){
    VkVertexInputBindingDescription bdesc {};
    bdesc.binding = 0;
    bdesc.stride = sizeof(Vertex2_t);
    bdesc.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
    return bdesc;
}
VkVertexInputBindingDescription vert3_getBindingDescription(){
    VkVertexInputBindingDescription bdesc {};
    bdesc.binding = 0;
    bdesc.stride = sizeof(Vertex3_t);
    bdesc.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
    return bdesc;
}
static VkVertexInputAttributeDescription adesc[3] {
    {
        .location = 0,
        .binding = 0,
        .format = VK_FORMAT_R32G32_SFLOAT,
        .offset = offsetof(Vertex2_t,pos)
    },

    {
        .location = 1,
        .binding = 0,
        .format = VK_FORMAT_R32G32B32_SFLOAT,
        .offset = offsetof(Vertex2_t,color),
    },
    {
        .location = 2,
        .binding = 0,
        .format = VK_FORMAT_R32G32_SFLOAT,
        .offset = offsetof(Vertex2_t,texCoord),
    }

};
VkVertexInputAttributeDescription* vert2_getAttributeDescription(){
   
    // adesc[0].binding = 0;
    // adesc[0].location = 0;
    // adesc[0].format = VK_FORMAT_R32G32_SFLOAT;
    // adesc[0].offset = offsetof(Vertex2_t,pos);

    // adesc[1].binding = 0;
    // adesc[1].location = 1;
    // adesc[1].format = VK_FORMAT_R32G32B32_SFLOAT;
    // adesc[1].offset = offsetof(Vertex2_t,color);
    return adesc;
}

static VkVertexInputAttributeDescription adesc3[3] {
    {
        .location = 0,
        .binding = 0,
        .format = VK_FORMAT_R32G32B32_SFLOAT,
        .offset = offsetof(Vertex3_t,pos)
    },

    {
        .location = 1,
        .binding = 0,
        .format = VK_FORMAT_R32G32B32_SFLOAT,
        .offset = offsetof(Vertex3_t,color),
    },
    {
        .location = 2,
        .binding = 0,
        .format = VK_FORMAT_R32G32_SFLOAT,
        .offset = offsetof(Vertex2_t,texCoord),
    }

};

VkVertexInputAttributeDescription* vert3_getAttributeDescription(){
    return adesc3;
}

#endif
#include "Vector3.h"
#include <stdint.h>

Vector3_32_t vAdd(Vector3_32_t* a, Vector3_32_t* b)
{
    Vector3_32_t res;
    res.nvec = a->nvec + b->nvec;
    return res;
}

Vector3_32_t vSub(Vector3_32_t* a, Vector3_32_t* b)
{
    Vector3_32_t res;
    res.nvec = a->nvec - b->nvec;
    return res;
}

Vector3_32_t vMult(Vector3_32_t* a, Vector3_32_t* b)
{
    Vector3_32_t res;
    res.nvec = a->nvec * b->nvec;
    return res;
}

Vector3_32_t vSqr(Vector3_32_t* a)
{
    return vMult(a,a);
}

uint64_t sqrMag(Vector3_32_t* a)
{
    Vector3_32_t res = vSqr(a);
    return res.coords.x + res.coords.y + res.coords.z;
}

bool lessEq(Vector3_32_t* a, Vector3_32_t* b)
{
    Vector3_32_t res;
    res.nvec = (a->nvec > b->nvec);
    return !res.coords.x && !res.coords.y && !res.coords.z;
}
bool greaterEq(Vector3_32_t* a, Vector3_32_t* b)
{
    Vector3_32_t res;
    res.nvec = (a->nvec < b->nvec);
    return !res.coords.x && !res.coords.y && !res.coords.z;
}

Vector3_32_t vAddInt(Vector3_32_t* a,int32_t b){
    Vector3_32_t val;
    val.nvec = a->nvec + b;
    return val;
}

Vector3_32_t vSubInt(Vector3_32_t* a,int32_t b){
    Vector3_32_t val;
    val.nvec = a->nvec - b;
    return val;
}

bool vEquals(Vector3_32_t* a, Vector3_32_t* b){
    Vector3_32_t res;
    res.nvec = (a->nvec != b->nvec);
    return !res.coords.x && !res.coords.y && !res.coords.z;
}


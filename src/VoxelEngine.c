#include "VoxelEngine.h"
#include <stdio.h>
#include <stdlib.h>
#include "Queue.h"
#include "Fifo.h"
#include "ChunkQueue.h"
#include "Vector3.h"
#include "WorldView.h"
#include "WorldGen.h"
#include "assert.h"
#include "TerGenSimplex.h"

void updateWorldCapacity(int32_t addedRangeXZ,int32_t addedRangeY);

static VoxelEngineState state;
static ChunkBucket_t* world;


uint64_t viewRadToDiam(uint64_t radius){
    return ( ( radius * 2 ) + 1 );
}


uint64_t viewXYZ(uint32_t x,uint32_t y,uint32_t z,uint32_t radiusXZ,uint32_t radiusY){
    return ( x * viewRadToDiam( radiusXZ ) * viewRadToDiam( radiusY ) ) + ( y * viewRadToDiam( radiusXZ ) ) + z;
}

void initVoxelEngine(uint32_t seed,const char* dataDir,uint16_t slen)
{
    state.dataDirLen = slen;
    state.views.head = NULL;
    state.dataDir = (char*)malloc(sizeof(char) * slen);
    memcpy(state.dataDir,dataDir,slen * sizeof(char));
    
    InitTGSNoiseSet(seed);
    InitializeWG(&generateTGS);
    // printf("initialized with datadir: %s\n",state.dataDir);
    state.world.capacity = 1;
    state.world.current = 1;
    state.world.chunkMem = calloc(state.world.capacity,sizeof(Chunk_t) + 31);
    state.world.chunks = (Chunk_t*)state.world.chunkMem;
    state.world.freeChunks = (uint32_t*) calloc(state.world.capacity,sizeof(uint32_t));
    state.world.currentFree = 0;
    state.run = 1;
    world = &state.world;
}

char* getDataDir()
{
    return state.dataDir;
}

uint16_t getDataDirLen()
{
    return state.dataDirLen;
}
void updateWorldCapacity(int32_t addedRangeXZ,int32_t addedRangeY)
{
    // world->capacity += VOLUME(addedRange); // can't do this because views have cubic storage of chunks
    world->capacity += viewRadToDiam(addedRangeXZ) * viewRadToDiam(addedRangeY) * viewRadToDiam(addedRangeXZ);
    if(world->capacity < world->memAlloc) return;
    void* nChunks = realloc(world->chunkMem,(uint64_t)(world->capacity * sizeof(Chunk_t) + 31));
    if(nChunks == NULL) {
        printf("FAILED TO ALLOCATE MEMORY FOR CHUNKS\n");//lol we'll just seg fault now
        assert(0);
    }
    printf("mem alloc'd to: %p\n",nChunks);
    uintptr_t diff = (uintptr_t)world->chunks - (uintptr_t)world->chunkMem;
    world->chunkMem = nChunks;
    nChunks = (void*)((uintptr_t)nChunks + (uintptr_t)31);
    nChunks = (void*)(((uintptr_t)nChunks) & ~(0x1F));
    printf("after corection: %p\n",nChunks);
    world->chunks = (Chunk_t*) nChunks;
    uintptr_t nDiff = (uintptr_t)world->chunks - (uintptr_t)world->chunkMem;
    if(diff != nDiff)
    {
        printf("repairing memory\n");
        memmove(world->chunks,world->chunkMem + diff,world->capacity * sizeof(Chunk_t));
    }
    world->freeChunks = (uint32_t*)realloc(world->freeChunks,(uint64_t)(world->capacity * sizeof(uint32_t)));//may want to calloc
    if(world->freeChunks == NULL)
    {
        printf("FAILED TO ALLOCATE MEMORY FOR FREE CHUNKS\n");
        assert(0);
    }
    printf("updated world capacity to: %i chunks: %lu bytes\n",world->capacity,world->capacity * sizeof(Chunk_t));
    world->memAlloc = world->capacity;
}

uint32_t getWorldCurrent()
{
    return world->current;
}

uint32_t getWorldCapacity()
{
    return world->capacity;
}
uint32_t getWorldMemAlloc()
{
    return world->memAlloc;
}

void buildLoadQueue(ViewID_t view,CheckChunk_t ccCB)
{
    generateQueue(view,ccCB);
}

static void nop(){}//omp hack
void loadChunks(ViewID_t view)
{
    // printf("loading chunks\n");
    if(view->loadCount - view->loadHead  > 0)
    {
        uint32_t limit = (view->loadCount - view->loadHead > LOAD_RENDER_WORK_LOAD) ? LOAD_RENDER_WORK_LOAD : view->loadCount - view->loadHead;
        limit += view->loadHead;
        #pragma omp parallel for default(none) shared(view,state,limit,world) schedule(static,LOAD_RENDER_WORK_LOAD/64)
        for(uint32_t i = view->loadHead; i < limit; i++)
        {   
            uint32_t chunkLoc;
            // if(world->chunks[chunkLoc].loaded && vEquals(&world->chunks[chunkLoc].id,&view->loadQueue[i])) continue;
            
            LinkedListNode_t* node = state.views.head;
            while(node)
            {
                if(node->value == view) {
                    node = node->nextNode;
                    continue;
                }
                ViewID_t oView = (ViewID_t)node->value;
                Vector3_32_t max = vAddInt(&oView->lastRenderedPos,oView->rangeXZ);
                Vector3_32_t min = vSubInt(&oView->lastRenderedPos,oView->rangeXZ);
                if(lessEq(&view->loadQueue[i],&max) && 
                greaterEq(&view->loadQueue[i],&min))
                {
                    chunkLoc = getViewChunkLoc(oView,&view->loadQueue[i]);
                    
                    if(chunkLoc && vEquals(&world->chunks[chunkLoc].id,&view->loadQueue[i]))
                    {
                        if(getViewChunkLoc(view,&view->loadQueue[i]))
                        {
                            #pragma omp critical
                            {
                                returnChunk(getViewChunkLoc(view,&view->loadQueue[i]));
                            }
                        }
                        addChunk(view,&view->loadQueue[i],chunkLoc);
                        world->chunks[chunkLoc].refCount++;
                        goto ML;
                    }  
                }
                node = node->nextNode;
            }
            chunkLoc = getViewChunkLoc(view,&view->loadQueue[i]);
            if(chunkLoc == 0 || --world->chunks[chunkLoc].refCount > 0){
                #pragma omp critical
                {
                    chunkLoc = getFreeChunk();   
                }
                addChunk(view,&view->loadQueue[i],chunkLoc);
            }else{
                world->chunks[chunkLoc].id = view->loadQueue[i];
                world->chunks[chunkLoc].refCount = 1;
            }
            
            loadOrGenerateChunk(view,&view->loadQueue[i]);
            world->chunks[chunkLoc].firstRender = 0;
            world->chunks[chunkLoc].loaded = 1;
            world->chunks[chunkLoc].dirty = 1;

            continue;
            ML:nop();
        }

        if((limit - view->loadHead + view->renderWrite) >= view->renderMax)
        {
            uint32_t firstWrite = view->renderMax - view->renderWrite;
            memcpy(&view->renderBuffer[view->renderWrite],&view->loadQueue[view->loadHead],firstWrite * sizeof(Vector3_32_t));
            view->renderWrite = 0;
            memcpy(&view->renderBuffer[view->renderWrite],&view->loadQueue[view->loadHead + firstWrite],(limit - view->loadHead - firstWrite) * sizeof(Vector3_32_t));
            view->renderWrite = limit - view->loadHead - firstWrite;
        }else{
            memcpy(&view->renderBuffer[view->renderWrite],&view->loadQueue[view->loadHead],(limit - view->loadHead) * sizeof(Vector3_32_t));
            view->renderWrite += limit - view->loadHead;
        }
        
        view->loadHead += limit - view->loadHead;
        
    }
    // printf("done loading chunks\n");
}

void renderChunks(ViewID_t view)
{
    // printf("rendering chunks\n");
    if(view->renderHead == view->renderWrite) return;
    uint32_t renderAmount;
    if(view->renderWrite < view->renderHead)
        renderAmount = view->renderWrite + view->renderMax - view->renderHead;
    else
        renderAmount = view->renderWrite - view->renderHead;
    // uint32_t limit = ((renderAmount > LOAD_RENDER_WORK_LOAD) ? (LOAD_RENDER_WORK_LOAD) : (renderAmount));
    uint32_t limit = renderAmount;
    limit += view->renderHead;
    #pragma omp parallel for default(none) shared(view,limit) schedule(dynamic,LOAD_RENDER_WORK_LOAD/64)
    for(uint32_t i = view->renderHead; i < limit; i++)
    {
        uint32_t index = (i >= view->renderMax) ? i - view->renderMax: i;
        if(getChunk(view,&view->renderBuffer[index])->dirty)
        renderChunk(view,&view->renderBuffer[index]);
    }
    view->renderHead += limit - view->renderHead;
    if(view->renderHead > view->renderMax) view->renderHead -= view->renderMax;
    // printf("done rendering chunks\n");
}

void fillVoxels(Voxel_t voxels[CHUNK_SIZE][CHUNK_SIZE][CHUNK_SIZE],Voxel_t value)
{
    // #pragma omp parallel for num_threads(32) schedule(static) collapse(3)
    for(int x = 0; x < CHUNK_SIZE; x++)
    for(int y = 0; y < CHUNK_SIZE; y++)
    for(int z = 0; z < CHUNK_SIZE; z++)
    {
        voxels[x][y][z] = value;
    }
}

void updateViewRange(ViewID_t view,uint32_t radXZ,uint32_t radY)
{
    updateWorldCapacity(-view->rangeXZ,-view->rangeY);
    int32_t oldRangeXZ = view->rangeXZ;//int so the checks in the for loop don't get fucky
    int32_t oldRangeY = view->rangeY;
    uint8_t checkXZ = oldRangeXZ > radXZ;
    uint8_t checkY = oldRangeY > radY;
    if(checkXZ || checkY)
    {
        int32_t xzStart = (checkXZ) ? oldRangeXZ : radXZ;
        int32_t yStart = (checkY) ? oldRangeY : radY;
        // printf("old range larger, freeing out of bounds chunks\n");
        for(int32_t x = -xzStart; x < xzStart; x++)
        for(int32_t y = -yStart; y < yStart; y++)
        for(int32_t z = -xzStart; z < xzStart; z++)
        {
            // printf("xyz: %i,%i,%i\n",x,y,z);
            if(checkXZ && (x < radXZ && x > -radXZ) || (z < radXZ && z > -radXZ)) continue;

            if(checkY && (y < radY && y > -radY)) continue;

            Vector3_32_t id;
            id.nvec = view->pos.nvec + (i32x4){x,y,z,0};
            
            // printf("chunk %i,%i,%i not within new range\n",id.x,id.y,id.z);
            if(getViewChunkLoc(view,&id) == 0 || --getChunk(view,&id)->refCount > 0) continue;
            // printf("chunk reference 0\n");
            returnChunk(getViewChunkLoc(view,&id));
            clearViewChunkLoc(view,&id);
        }
    }
    
    updateWorldCapacity(radXZ,radY);
    uint32_t diameter = viewRadToDiam(radXZ);
    uint32_t ydiam = viewRadToDiam(radY);
    uint32_t* nChunks = (uint32_t*)calloc((diameter * ydiam * diameter),sizeof(uint32_t));
    WorldView_t dummy;
    dummy.chunks = nChunks;
    dummy.rangeXZ = radXZ;
    dummy.rangeY = radY;
    int32_t rangeXZ = (oldRangeXZ > radXZ) ? radXZ : oldRangeXZ;
    int32_t rangeY = (oldRangeY > radY) ? radY : oldRangeY;
    // printf("copying chunks to dummy for transfer\n");
    for(int32_t x = -rangeXZ; x < rangeXZ; x++)
    for(int32_t y = -rangeY; y < rangeY; y++)
    for(int32_t z = -rangeXZ; z < rangeXZ; z++)
    {
        
        Vector3_32_t id;
        
        id.nvec = view->pos.nvec + (i32x4){x,y,z,0};
        // printf("chunk %i,%i,%i adding...\n",id.x,id.y,id.z);
        if(getViewChunkLoc(view,&id) == 0) continue;
        addChunk(&dummy,&id,getViewChunkLoc(view,&id));
    }
    view->rangeXZ = radXZ;
    view->rangeY = radY;
    free(view->chunks);
    view->chunks = nChunks;
    // printf("chunks copied and moved\n");
    
    Vector3_32_t* nQueue = (Vector3_32_t*) malloc(VOLUME(view->rangeXZ) * sizeof(Vector3_32_t));
    free(view->loadQueue);
    view->loadQueue = nQueue;
    generateZeroQueue(view);
    Vector3_32_t* nBuffer = (Vector3_32_t*) calloc(VOLUME(view->rangeXZ),sizeof(Vector3_32_t));
    uint32_t renderWrite = 0;
    if(view->renderHead != view->renderWrite)
    {
        if( VOLUME(view->rangeXZ) >= VOLUME(oldRangeXZ) )
        {
            if(view->renderHead > view->renderWrite)
            {
                memcpy(nBuffer,&view->renderBuffer[view->renderHead],view->renderMax - view->renderHead * sizeof(Vector3_32_t));
                memcpy(&nBuffer[view->renderMax - view->renderHead],view->renderBuffer,view->renderWrite * sizeof(Vector3_32_t));
                renderWrite = (view->renderMax - view->renderHead) + view->renderWrite;
            }else{
                memcpy(nBuffer,&view->renderBuffer[view->renderHead],view->renderWrite - view->renderHead * sizeof(Vector3_32_t));
                renderWrite = view->renderWrite - view->renderHead;
            }
        }else{
            uint32_t index = 0;
            uint32_t sqMag = view->rangeXZ * view->rangeXZ;
            uint32_t limit = (view->renderWrite < view->renderHead) ? view->renderWrite + view->renderMax : view->renderWrite;
            for(int i = view->renderHead; i < limit; i++)
            {
                uint32_t buffInd = i;
                if(buffInd > view->renderMax) buffInd -= view->renderMax;

                if(sqrMag(&view->renderBuffer[buffInd]) < sqMag && view->renderBuffer[buffInd].coords.y < view->rangeY)
                {
                    nBuffer[index++] = view->renderBuffer[i];
                    if(index >= VOLUME(view->rangeXZ)) break; //should never happen, but, just in case
                }
            }
            renderWrite = index;
        }
    }

    free(view->renderBuffer);
    view->renderBuffer = nBuffer;
    view->renderMax = VOLUME(view->rangeXZ);
    view->queueDirty = 1;
    view->loadHead = 0;
    view->loadCount = 0;
    view->renderHead = 0;
    view->renderWrite = renderWrite;
    // printf("finished updating view size\n");
}

static void addToRenderBuffer(ViewID_t view, Vector3_32_t *id)
{
    if(view->renderWrite == view->renderMax)
    {
        view->renderWrite = 0;
    }
    view->renderBuffer[view->renderWrite++] = *id;
}

void renderChunk(ViewID_t view,Vector3_32_t* id)
{
    // printf("rendering chunk %i,%i,%i\n",id->coords.x, id->coords.y, id->coords.z);   
    Vector3_32_t rightID,topID,backID;
    rightID = *id;
    rightID.coords.x++;
    topID = *id;
    topID.coords.y++;
    backID = *id;
    backID.coords.z++;
    Chunk_t *chunk,*right,*top,*back;
    chunk = getChunk(view,id);
    if(abs(id->coords.x) == view->rangeXZ)
    {
        addToRenderBuffer(view,id);
        return;
    }else{
        right = getChunk(view,&rightID);
    }
    if(abs(id->coords.y) == view->rangeY)
    {
        addToRenderBuffer(view,id);
        return;
    }else{
        top = getChunk(view,&topID);
    }
    if(abs(id->coords.z) == view->rangeXZ)
    {
        addToRenderBuffer(view,id);
        return;
    }else{
        back = getChunk(view,&backID);
    }
    if(!right->loaded || !top->loaded || !back->loaded)
    {
        if(abs(id->coords.x) <= view->rangeXZ && abs(id->coords.z) <= view->rangeXZ && abs(id->coords.y) <= view->rangeY) addToRenderBuffer(view,id);
        return;
    }
    chunk->wasAir = chunk->isAir;
    // VoxelMesh_t meshTemp[4096];
    struct MeshTemp{
        uint16_t locs[4096];
        uint64_t types[4096];
        uint8_t faces[4096];
    } meshTemp;
    int count = 0;

    // #pragma omp simd 
    for(uint16_t index = 0; index < CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE; index++)
    {
        // printf("doing index: %i\n",index);
        uint8_t x = ((index >> 8) & 0xF);
        uint8_t y = ((index >> 4) & 0xF);
        uint8_t z = (index & 0xF);
        VoxelMesh_t mesh ={
            .location = index,
            
        };
        mesh.types.vx.type = chunk->voxels.voxels[x][y][z];
        mesh.types.vx.leftType = (x == 0xF) ? right->voxels.voxels[0][y][z] : chunk->voxels.voxels[x + 1][y][z];
        mesh.types.vx.bottomType = (y == 0xF) ? top->voxels.voxels[x][0][z] : chunk->voxels.voxels[x][y + 1][z];
        mesh.types.vx.frontType = (z == 0xF) ? back->voxels.voxels[x][y][0] : chunk->voxels.voxels[x][y][z + 1];
        
        
        uint8_t leftNeq = mesh.types.vx.leftType.type != mesh.types.vx.type.type;
        uint8_t botNeq = mesh.types.vx.bottomType.type != mesh.types.vx.type.type;
        uint8_t frontNeq = mesh.types.vx.frontType.type != mesh.types.vx.type.type;
        //left  
        mesh.faces.face.leftFace = mesh.types.vx.type.bitfield.nopaque && mesh.types.vx.leftType.type != 0xFFFF && leftNeq && !mesh.types.vx.leftType.bitfield.item; 

        //bottom   
        mesh.faces.face.bottomFace = mesh.types.vx.type.bitfield.nopaque && mesh.types.vx.bottomType.type != 0xFFFF && botNeq && !mesh.types.vx.bottomType.bitfield.item;
        
        //front   
        mesh.faces.face.frontFace = mesh.types.vx.type.bitfield.nopaque && mesh.types.vx.frontType.type != 0xFFFF && frontNeq && !mesh.types.vx.frontType.bitfield.item;

        
        uint8_t airBlock = mesh.types.vx.type.type == 0xFFFF;
        if (!airBlock){
            // uint8_t meshChecks = !mesh.types.vx.type.bitfield.item;
            mesh.faces.face.rightFace = !mesh.types.vx.type.bitfield.item && mesh.types.vx.leftType.bitfield.nopaque;
            mesh.faces.face.topFace =  !mesh.types.vx.type.bitfield.item  && mesh.types.vx.bottomType.bitfield.nopaque;
            mesh.faces.face.backFace = !mesh.types.vx.type.bitfield.item  && mesh.types.vx.frontType.bitfield.nopaque;
        }
        if(mesh.faces.faces || (mesh.types.vx.type.bitfield.item && !airBlock))
        {
            // printf("adding mesh value at %i,%i,%i, item: %i face values: %i,%i,%i,%i,%i,%i\n",x,y,z,blockType.voxel.bitfield.item,mesh.backFace, mesh.bottomFace, mesh.frontFace, mesh.leftFace, mesh.rightFace, mesh.topFace);
            // #pragma omp critical
            // {
               meshTemp.locs[count] = mesh.location;
               meshTemp.faces[count] = mesh.faces.faces;
               meshTemp.types[count++] = mesh.types.types;
            // }
        }


        // && blockType.voxel.type != 0xFFFF
        //!blockType.voxel.bitfield.item  &&     
    }
    chunk->isAir = count == 0;
    // printf("mesh count: %i\n",count);
    if(!chunk->isAir || (!chunk->wasAir && chunk->firstRender)){
        view->updateCB(id,meshTemp.locs,meshTemp.types,meshTemp.faces,count);//cb function should copy out data since it's on stack
    }
    chunk->dirty = 0;
    chunk->firstRender = 1;
}

uint32_t getFreeChunk()
{
    uint32_t chunkLoc = (world->currentFree > 0) ? world->freeChunks[--world->currentFree] : world->current++;
    world->chunks[chunkLoc].refCount = 1;
    return chunkLoc;
}

uint32_t* DEBUG_GET_WORLD_FREE_ARRAY()
{
    return world->freeChunks;
}

void returnChunk(uint32_t chunkLoc)
{
    world->chunks[chunkLoc].refCount = 0;
    world->chunks[chunkLoc].loaded = 0;
    world->freeChunks[world->currentFree++] = chunkLoc;
}

uint32_t availableFreeChunks()
{
    return world->currentFree;
}

Chunk_t* getChunk(ViewID_t view,Vector3_32_t* id)//no checking if the id is within the view, you have to do that yourself here or you'll be reading the wrong chunk
{
    
    return &world->chunks[view->chunks[viewIndex(view,id)]];
}
void clearViewChunkLoc(ViewID_t view,Vector3_32_t* id)
{
    view->chunks[viewIndex(view,id)] = 0;
}

uint32_t getViewChunkLoc(ViewID_t view,Vector3_32_t* id)
{
    
    return view->chunks[viewIndex(view,id)];
}

uint32_t viewIndex(ViewID_t view,Vector3_32_t* id)
{
    int32_t mod = viewRadToDiam(view->rangeXZ);
    int32_t ymod = viewRadToDiam(view->rangeY);
    int32_t x,y,z;
    x = id->coords.x % mod;
    if(x < 0) x += mod;
    if(y < 0) y += ymod;
    if(z < 0) z += mod;
    return viewXYZ(x,y,z,view->rangeXZ,view->rangeY);
}

void addChunk(ViewID_t view,Vector3_32_t* id,uint32_t chunkLoc)
{
    //FOR TESTING, MAKING SURE WE NEVER WRITE TO CHUNK 0 (GOD CHUNK)
    //=========================
    // assert(chunkLoc != 0);
    //=========================
    // printf("id rel xyz: %li,%li,%li\n",index.x % mod,index.y % mod, index.z % mod);
    // printf("adding chunk to view at index: %i\n",viewXYZ(index.x % mod, index.y % mod, index.z %mod, view->range));
    world->chunks[chunkLoc].id = *id;
    view->chunks[viewIndex(view,id)] = chunkLoc;
}
// #include <unistd.h>
ViewID_t createView(uint32_t radXZ,uint32_t radY,Vector3_32_t pos,UpdateCB_t updateCB)
{
    printf("createing view at pos %i,%i,%i\n",pos.coords.x,pos.coords.y,pos.coords.z);
    ViewID_t view = (ViewID_t)malloc(sizeof(WorldView_t));
    if(view == NULL)
    {
        printf("view null\n");
        assert(0);
    }
    view->rangeY = radY;
    view->rangeXZ = radXZ;
    view->pos = pos;
    view->lastPlayerPos = pos;
    view->lastRenderedPos = pos;
    uint32_t diameter = viewRadToDiam(radXZ);
    uint32_t yDiam = viewRadToDiam(radY);
    view->chunks = (uint32_t*)calloc((diameter * yDiam * diameter),sizeof(uint32_t));//figure out how to make this smaller
    if(view->chunks == NULL) {
        printf("view chunks null\n");
        assert(0);
    }
    view->updateCB = updateCB;
    view->loadCount = 0;
    view->loadHead = 0;
    view->renderHead = 0;
    view->queueHead = 0;
    view->loadQueue = (Vector3_32_t*) malloc(VOLUME(view->rangeXZ) * sizeof(Vector3_32_t));
    view->renderBuffer = (Vector3_32_t*) calloc(VOLUME(view->rangeXZ),sizeof(Vector3_32_t));
    view->renderMax = VOLUME(view->rangeXZ);
    view->renderWrite = 0;
    view->queueDirty = 1;
    if(view->loadQueue == NULL)
    {
        printf("view load queue null\n");
        assert(0);
    }
    // printf("updating world capacity\n");
    updateWorldCapacity(view->rangeXZ,view->rangeY);
    view->zeroQueue = NULL;
    // printf("generating queue\n");
    generateZeroQueue(view);
    
    // printf("adding to linked list\n");
    // sleep(3);
    LL_add(&state.views,view);
    // printf("finished creating view\n");
    return view;
}
static void freeView(ViewID_t view)
{
    free(view->chunks);
    free(view->zeroQueue);
    free(view->loadQueue);
    free(view);
}
void destroyVE()
{
    free(state.world.chunkMem);
    LL_forEach(state.views.head,(LLForEach_t)freeView);
    LL_free(&state.views);
    DestroyTGS();
}

LinkedListNode_t* getViewHead(){
    return state.views.head;
}

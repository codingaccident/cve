#include "Fifo.h"

static bool enqueue(void* queue, void* data, QueueElementSize_t size)//return false if we were going to overflow, and do not overwrite data
{
    SELF(queue,fifo,Fifo_t*,Queue_t*);
    if(fifo->tail == fifo->head) {
        return false;
    }
    memcpy(fifo->data + (fifo->tail++ * fifo->elementSize), data, fifo->elementSize);
    if(fifo->tail >= fifo->maxElements) fifo->tail = 0;
    return true;
}

static QueueElementSize_t dequeue(void* queue, void* data)
{
    SELF(queue,fifo,Fifo_t*,Queue_t*);
    memcpy(data,fifo->data + (fifo->head++ * fifo->elementSize),fifo->elementSize);
    if(fifo->head >= fifo->maxElements) fifo->head = 0;
    return fifo->elementSize;
}

static uint32_t count(void* queue)
{
    return ((Fifo_t*)queue)->tail-((Fifo_t*)queue)->head;
}

static QueueInterface_t self = {
        .enqueue = enqueue,
        .dequeue = dequeue,
        .count = count
    };

Queue_t* initFifo(Fifo_t* queue, QueueElementSize_t elementSize, size_t maxElements)
{
    queue->elementSize = elementSize;
    queue->maxElements = maxElements;
    queue->data = malloc(maxElements * elementSize);
    queue->tail = 0;
    queue->queue.interface = &self;
    queue->queue.self = queue;
    return &queue->queue;
}

void destroyFifo(Fifo_t* queue)
{
    free(queue->data);
}
#include "ChunkQueue.h"
#include <stdint.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "Vector3.h"

uint32_t remainingElements(ChunkQueue_t* chunkQueue)
{
    return chunkQueue->count - chunkQueue->head;
}

Vector3_32_t dequeue(ChunkQueue_t* chunkQueue)
{
    return chunkQueue->data[chunkQueue->head++];
}
void generateZeroQueue(WorldView_t* view)
{
    if(view->zeroQueue != NULL)
        free(view->zeroQueue);
    Vector3_32_t *zeroQueue = (Vector3_32_t*) malloc(( VOLUME(view->rangeXZ) ) * sizeof(Vector3_32_t));
    uint32_t i = 0;
    uint32_t sqradius = view->rangeXZ * view->rangeXZ;

    for(int32_t y =  0; y <= view->rangeY; y++)
    for(int32_t x =  0; x <= view->rangeXZ; x++)
    for(int32_t z =  0; z <= view->rangeXZ; z++)
    {
        // printf("attempting sphere: %i\n",i);
        uint32_t sqmagnitude = (x * x) + (y * y) + (z * z);
        if(sqmagnitude > sqradius) continue;
        zeroQueue[i++].nvec = (i32x4){x,y,z,0};
        uint8_t xnzero = x != 0;
        uint8_t ynzero = y != 0;
        uint8_t znzero = z != 0;

        if(xnzero)
        {
            zeroQueue[i++].nvec = (i32x4){-x,y,z,0};
        }
        if(ynzero)
        {
            zeroQueue[i++].nvec = (i32x4){x,-y,z,0};
        }
        if(xnzero && ynzero)
        {
            zeroQueue[i++].nvec = (i32x4){-x,-y,z,0};
        }
        if(znzero)
        {
            zeroQueue[i++].nvec = (i32x4){x,y,-z,0};
        }
        if(znzero && xnzero)
        {
            zeroQueue[i++].nvec = (i32x4){-x,y,-z,0};
        }
        if(znzero && ynzero)
        {
            zeroQueue[i++].nvec = (i32x4){x,-y,-z,0};
        }
        if(znzero && ynzero && xnzero)
        {
            zeroQueue[i++].nvec = (i32x4){-x,-y,-z,0};
        }
    }
    view->zeroQueue = zeroQueue;
    view->zeroCount = i;
}

void generateQueue(WorldView_t* view,CheckChunk_t checkChunk)
{
    Vector3_32_t* total = view->loadQueue;
    uint32_t count = 0;
    Vector3_32_t* zeroQueue = view->zeroQueue;
    Vector3_32_t offsetPos = vSub(&view->pos,&view->lastPlayerPos);
    offsetPos = vAdd(&view->pos,&offsetPos);

    view->lastPlayerPos = view->pos;
    view->lastRenderedPos = offsetPos;

    while(count < LOAD_RENDER_WORK_LOAD)
    {
        uint32_t limit = (view->zeroCount - view->queueHead > LOAD_RENDER_WORK_LOAD) ? LOAD_RENDER_WORK_LOAD : view->zeroCount - view->queueHead;
        limit += view->queueHead;
        #pragma omp parallel for default(none) shared(view,limit,zeroQueue,offsetPos,checkChunk,total) schedule(static,LOAD_RENDER_WORK_LOAD/64) shared(count)
        for(int j = view->queueHead; j < limit; j++)
        {
            if(count == LOAD_RENDER_WORK_LOAD) continue;
            Vector3_32_t cpos = vAdd(&zeroQueue[j],&offsetPos);
            if(checkChunk(view,cpos.nvec))
            {
                #pragma omp critical
                {
                if(count != LOAD_RENDER_WORK_LOAD)
                total[count++] = cpos;
                }
            }
        }
    
        view->queueHead += limit - view->queueHead;
        if(view->queueHead == view->zeroCount)
        {
            view->queueDirty = 0;
            break;
        }
    }
    view->loadCount = count;
    view->loadHead = 0;
    
}
#include "WorldGen.h"
#include "WorldView.h"
#include "Vector3.h"
#include "VoxelEngine.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "sys/stat.h"
#include "Voxel.h"
#ifdef _WIN64
#include <direct.h>
#endif

static WorldGenerator_t generator;
static void buildFileName(Vector3_32_t* id,char *fileName);
void InitializeWG(GenerateFun_t generate)
{
    generator.generate = generate;
}

void loadOrGenerateChunk(WorldView_t* view, Vector3_32_t *id){
    FILE *chunkFile;
    // printf("creating file name\n");
    char *fileName = (char*) malloc(getDataDirLen() + 50);
    buildFileName(id,fileName);
    // printf("file name: %s\n",fileName);
    struct stat buffer;
    uint8_t gen = stat(fileName,&buffer);
   
    Chunk_t* chunk = getChunk(view,id);
    // printf("opened file, gen: %i\n",gen);
    if(gen == 0)//success
    {
        chunkFile = fopen(fileName,"rb");
        fread(chunk->voxels.voxels,sizeof(Voxel_t),CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE,chunkFile);
        fclose(chunkFile);
        // free(fileName);
        return;
    }
    generator.generate(view,id);
    free(fileName);
}

void saveChunk(WorldView_t* view,Vector3_32_t* id)
{   
    FILE *chunkFile;
    Chunk_t* chunk = getChunk(view,id);
    char *fileName = (char*) malloc(getDataDirLen() + 50);
    buildFileName(id,fileName);
    sprintf(&fileName[getDataDirLen() + 8],"Chunk_%i,%i,%i.bin",id->coords.x,id->coords.y,id->coords.z);
    chunkFile = fopen(fileName,"w+b");
    fwrite(chunk->voxels.voxels,sizeof(Voxel_t),CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE,chunkFile);
    fclose(chunkFile);
    free(fileName);
}

static void buildFileName(Vector3_32_t* id,char *fileName) //also creates directories up to file
{
    // fileName = (char*) malloc(getDataDirLen() + 50);
    memcpy(&fileName[0],getDataDir(),getDataDirLen());
    // printf("filling file name\n");
    sprintf(&fileName[getDataDirLen()],"/chunks/");
    #ifdef _WIN64
    _mkdir(fileName);//only makes one directory, because why the fuck would we want to make all the directories passed in this path when we could uselessly try and make only the last one? Fuck you Bill Gates. Eat shit and die.
    #else
    mkdir(fileName,0755);
    #endif
    sprintf(&fileName[getDataDirLen() + 8],"Chunk_%i,%i,%i.bin",id->coords.x,id->coords.y,id->coords.z);
   
}
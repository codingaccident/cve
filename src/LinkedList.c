#include "LinkedList.h"
#include <stdint.h>
#include <stddef.h>
#include <stdlib.h>

void LL_add(LinkedList_t *list,void *value)
{
    LinkedListNode_t *llnode = (LinkedListNode_t*)malloc(sizeof(LinkedListNode_t));
    llnode->value = value;
    llnode->nextNode = list->head;
    list->head = llnode;
}

void *LL_remove(LinkedList_t *list,void *value)
{
    LinkedListNode_t* prevNode = NULL;
    LinkedListNode_t* node = list->head;
    while(node)
    {
        if(node->value == value)
        {
            if(node == list->head)
            {
                list->head = list->head->nextNode;
            }else{
                if(prevNode)
                prevNode->nextNode = node->nextNode;
            }
//            void* value = node->value;
            free(node);
            return value;//TODO this is weird
        }
        node = node->nextNode;
    }
    return NULL;
}

void LL_forEach(LinkedListNode_t *head,LLForEach_t func)
{
    while(head)
    {
        func(head->value);
        head = head->nextNode;
    }
}

LinkedListNode_t* LL_search(LinkedListNode_t *head,LLSearch_t search,void *searchData)
{
    while(head)
    {
        if(search(head->value,searchData)) return head;
        head = head->nextNode;
    }
    return NULL;
}

uint32_t LL_len(LinkedList_t* list)
{
    uint32_t len = 0;
    LinkedListNode_t* head = list->head;
    while(head) {
        len++;
        head = head->nextNode;
    }
    return len;
}

void LL_free(LinkedList_t* list)
{
    while(list->head)
    {
        LinkedListNode_t* head = list->head->nextNode;
        free(list->head);
        list->head = head;
    }
}
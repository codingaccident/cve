#ifndef VOXEL_ENGINE_API
#define VOXEL_ENGINE_API
#include "VoxelEngine.h"

typedef struct {
    uint32_t genTime;
    uint32_t renderTime;
    uint32_t queueTime;
    uint32_t totalTime;
    uint32_t loadHead;
    uint32_t renderHead;
    uint32_t queueSize;
    Vector3_32_t pos;
    uint32_t memSize;
    uint32_t totalChunk;
    uint32_t curentChunk;
} Metrics_t;
#ifdef __cplusplus
extern "C"{
#endif
#define EXPORT __attribute__((visibility("default")))
//all XYZ coords are in chunk space, i.e. worldspace / CHUNK_SIZE, which is currently 16, unless otherwise stated
EXPORT void cve_Initialize(const char* dataDir,uint16_t slen);
EXPORT ViewID_t cve_CreateView(int radXZ, int radY, int x, int y, int z,UpdateCB_t updateCB);//returns ViewID
EXPORT void cve_UpdateViewPosition(ViewID_t viewID,int x, int y, int z);
EXPORT unsigned short cve_GetVoxel(int x, int y, int z);//SLOW FUNCTION, FINDS A VIEW THAT CAN SERVE THIS //worldspace
EXPORT unsigned short cve_GetVoxelView(ViewID_t viewID, int x, int y, int z); //worldspace
EXPORT void cve_SetVoxel(unsigned short value, int x, int y, int z);//SLOW FUNCTION, FINDS A VIEW THAT CAN SERVICE THIS // worldspace
EXPORT void cve_SetVoxelView(unsigned short value, ViewID_t viewID, int x, int y, int z);//worldspace
EXPORT void cve_UpdateViewRange(ViewID_t viewID,unsigned int radXZ,unsigned int radY);
EXPORT void cve_Shutdown();//free all resources
Metrics_t DEBUG_GET_METRICS();
#ifdef __cplusplus
}
#endif





#endif
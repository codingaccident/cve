#ifndef TERGEN_SIMPLEX_H
#define TERGEN_SIMPLEX_H
#include "WorldGen.h"
#include "WorldView.h"
#include "Vector3.h"

typedef struct {
    WorldGenerator_t generator;

} TerGenSimplex_t;
#ifdef __cplusplus
extern "C"{
#endif
void generateTGS(WorldView_t* view,Vector3_32_t* id);
void DestroyTGS();
void InitTGSNoiseSet(uint32_t seed);
#ifdef __cplusplus
}
#endif
#endif
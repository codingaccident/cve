#ifndef LIST_H
#define LIST_H

#include <stdint.h>
#include <stdio.h>
#include <assert.h>

#define LIST(type) \
typedef struct {    \
    type *data;     \
    uint32_t capacity;   \
    uint32_t count;        \
} type## _List_t;           \
void type##_listInit(type##_List_t *list,uint32_t size) \
{   \
    list->data = (type*) malloc(sizeof(type) * size); \
    list->capacity = size;  \
    list->count = 0;    \
}   \
void type## _listAdd( type##_List_t *list, type value,uint32_t index) \
{       \
    if(list->capacity == list->count)    \
    {       \
        list->capacity *= 2;    \
        void* nLoc = realloc(list->data,sizeof( type ) * list->capacity);    \
        if(!nLoc)   \
        {           \
            printf("list resize failed\n"); \
            assert(0);              \
        }else list->data = ( type *)nLoc;       \
    }       \
    memcpy(&list->data[index+1],&list->data[index],(list->count - index) * sizeof( type ));        \
    list->data[index] = value;  \
    list->count++;        \
}   \


#endif
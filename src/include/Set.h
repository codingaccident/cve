#ifndef INT_SET_H
#define INT_SET_H
#include <stdbool.h>
#include <stdint.h>
#include "LinkedList.h"

#define DEFINESET(name, type) typedef struct{ \
    LinkedList_t data;\
}name##_Set_t; \
static bool name##_set_search(const type *value,const type *search)    \
{   \
    return *value == *search;\
}   \
bool name##_set_contains(name##_Set_t* set,type *value) \
{   \
    return LL_search(set->data.head,(LLSearch_t)name##_set_search,value);\
}   \
bool name##_set_add(name##_Set_t* set,type *value) \
{   \
    if(!name##_set_contains(set,value))\
    {   \
        LL_add(&set->data,value);\
        return true;\
    }    \
    return false;    \
}   \



DEFINESET(Int32,int32_t)
DEFINESET(Uint32,uint32_t)

// bool contains(Int32_Set_t* set,int32_t value);

#endif
#ifndef FIFO_H
#define FIFO_H
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "Queue.h"

typedef struct {
    Queue_t queue;
    void* data;
    uint32_t head;
    uint32_t tail;
    QueueElementSize_t elementSize;
    uint32_t maxElements;
} Fifo_t;

Queue_t* initFifo(Fifo_t* queue,QueueElementSize_t elementSize, size_t maxElements);
void destroyFifo(Fifo_t* queue);

#endif
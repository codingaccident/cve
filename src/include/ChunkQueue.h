#ifndef CHUNK_QUEUE_H
#define CHUNK_QUEUE_H
#include <math.h>
#include "Vector3.h"
#include "VoxelEngine.h"
#include "WorldView.h"

#define VOLUME(radius) ((4.0/3.0) * M_PI * (double)(radius * radius * radius))
#define EIGHTH_VOLUME(radius) (VOLUME(radius)/8.0)

typedef struct{
    Vector3_32_t* data;
    uint32_t head;
    uint32_t count;
} ChunkQueue_t;

#ifdef __cplusplus
extern "C"{
#endif
Vector3_32_t dequeue(ChunkQueue_t* chunkQueue);
uint32_t remainingElements(ChunkQueue_t* chunkQueue);
void generateQueue(WorldView_t* view,CheckChunk_t checkChunk);
void generateZeroQueue(WorldView_t* view);
#ifdef __cplusplus
}
#endif
#endif
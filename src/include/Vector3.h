#ifndef VECTOR3
#define VECTOR3
#include <stdint.h>
#include <immintrin.h>
#include <stdbool.h>

typedef int i32x4 __attribute__ ((vector_size(16)));

typedef union {
        __m128i vec;
        i32x4 nvec;
        struct {
            __attribute__((aligned(4))) int32_t x;
            __attribute__((aligned(4))) int32_t y;
            __attribute__((aligned(4))) int32_t z;
            __attribute__((aligned(4))) int32_t v;
           
        } coords;
    } Vector3_32_t;


#ifdef __cplusplus
extern "C"{
#endif

Vector3_32_t vAdd(Vector3_32_t* a, Vector3_32_t* b);
Vector3_32_t vAddInt(Vector3_32_t* a,int32_t b);
Vector3_32_t vSub(Vector3_32_t* a, Vector3_32_t* b);
Vector3_32_t vSubInt(Vector3_32_t* a,int32_t b);
Vector3_32_t vMult(Vector3_32_t* a, Vector3_32_t* b);
Vector3_32_t vSqr(Vector3_32_t* a);
uint64_t sqrMag(Vector3_32_t* a);
bool lessEq(Vector3_32_t* a, Vector3_32_t* b);
bool greaterEq(Vector3_32_t* a, Vector3_32_t* b);
bool vEquals(Vector3_32_t* a, Vector3_32_t* b);

#ifdef __cplusplus
}
#endif

#endif
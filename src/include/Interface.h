#ifndef INTERFACE_H
#define INTERFACE_H

#define SELF(oldname,newname,type,interface) type newname = (type)((interface)oldname)->self
#define CREATE_INTERFACE(interfaceName) \
typedef struct  { \
    void* self; \
    interfaceName##Interface_t* interface; \
} interfaceName##_t;

#endif
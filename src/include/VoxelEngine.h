
#ifndef VOXEL_ENGINE_H
#define VOXEL_ENGINE_H
#include <stdint.h>
#include "Vector3.h"
#include "WorldView.h"
#include "Voxel.h"
#include "LinkedList.h"
#define CHUNK_SIZE 16

typedef unsigned short u16x8 __attribute__ ((vector_size(16)));
typedef unsigned short u16x16 __attribute__ ((vector_size(32)));

typedef __attribute__((aligned (32))) struct {
    union{
        Voxel_t voxels[CHUNK_SIZE][CHUNK_SIZE][CHUNK_SIZE];
        // u16x8 voxvec[CHUNK_SIZE][CHUNK_SIZE][CHUNK_SIZE/8];
        u16x16 voxvec[CHUNK_SIZE][CHUNK_SIZE];
    } voxels;
    Vector3_32_t id;
    uint16_t refCount;
    uint8_t isAir : 1;
    uint8_t wasAir : 1;
    uint8_t firstRender : 1;
    uint8_t loaded : 1;
    uint8_t dirty : 1;
    char padding[25];
}  Chunk_t;

typedef struct {
    Chunk_t *chunks;
    void* chunkMem;
    uint32_t capacity;
    uint32_t memAlloc;
    uint32_t current;
    uint32_t currentFree;
    uint32_t *freeChunks;
} ChunkBucket_t;

typedef struct {
    Vector3_32_t id;
} ChunkID_t;

typedef struct {
    ChunkBucket_t world;
    LinkedList_t views;
    uint8_t run;
    char* dataDir;
    uint16_t dataDirLen;
} VoxelEngineState;

#define ViewID_t WorldView_t*
#define LOAD_RENDER_WORK_LOAD 1000
#ifdef __cplusplus
extern "C"{
#endif
uint32_t getFreeChunk();
uint64_t viewRadToDiam(uint64_t radius);
uint64_t viewXYZ(uint32_t x,uint32_t y,uint32_t z,uint32_t radiusXZ,uint32_t radiusY);
ViewID_t createView(uint32_t radXZ,uint32_t radY,Vector3_32_t pos,UpdateCB_t updateCB);
Chunk_t* getChunk(ViewID_t view,Vector3_32_t* id);
void fillVoxels(Voxel_t voxels[CHUNK_SIZE][CHUNK_SIZE][CHUNK_SIZE],Voxel_t value);
void addChunk(ViewID_t view,Vector3_32_t* id,uint32_t chunkLoc);
void initVoxelEngine(uint32_t seed,const char* dataDir,uint16_t slen);
void buildLoadQueue(ViewID_t view,CheckChunk_t ccCB);
void loadChunks(ViewID_t view);
void renderChunk(ViewID_t view,Vector3_32_t* id);
void renderChunks(ViewID_t view);
void updateViewRange(ViewID_t view,uint32_t radXZ,uint32_t radY);
void returnChunk(uint32_t chunkLoc);
void destroyVE();
char* getDataDir();
uint16_t getDataDirLen();
uint32_t getViewChunkLoc(ViewID_t view,Vector3_32_t* id);
uint32_t getWorldCapacity();
uint32_t getWorldCurrent();
uint32_t getWorldMemAlloc();
uint32_t availableFreeChunks();
uint32_t* DEBUG_GET_WORLD_FREE_ARRAY();
void clearViewChunkLoc(ViewID_t view,Vector3_32_t* id);
uint32_t viewIndex(ViewID_t view,Vector3_32_t* id);
LinkedListNode_t* getViewHead();
#ifdef __cplusplus
}
#endif
#endif
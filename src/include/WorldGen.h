#ifndef WORLD_GEN_H
#define WORLD_GEN_H
#include "Interface.h"
#include "WorldView.h"
#include "VoxelEngine.h"

typedef float f32x8 __attribute__ ((vector_size(32)));
typedef float f32x16 __attribute__ ((vector_size(64)));
typedef uint16_t u16x16 __attribute__ ((vector_size(32)));
typedef int32_t i32x8 __attribute__ ((vector_size(32)));
typedef uint32_t u32x8 __attribute__ ((vector_size(32)));
typedef uint32_t u32x16 __attribute__ ((vector_size(64)));
typedef int32_t i32x16 __attribute__ ((vector_size(64)));
typedef void (*GenerateFun_t)(WorldView_t* view,Vector3_32_t *id);
typedef struct {
    GenerateFun_t generate;
} WorldGenerator_t;

#ifdef __cplusplus
extern "C"{
#endif
void loadOrGenerateChunk(WorldView_t* view, Vector3_32_t *id);
void InitializeWG(GenerateFun_t generate);
void saveChunk(WorldView_t* view,Vector3_32_t* id);
#ifdef __cplusplus
}
#endif
#endif
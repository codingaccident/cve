#ifndef WORLD_VIEW_H
#define WORLD_VIEW_H
#include "Vector3.h"
#include <stdint.h>
#include <stdbool.h>
#include "Voxel.h"
    //     public enum BlockFace{
    //     TOP = 1,
    //     FRONT = 3,
    //     BACK = 2,
    //     BOTTOM = 4,
    //     RIGHT = 0,
    //     LEFT = 5
    // }
typedef struct {
    uint16_t location;
    union{
        uint64_t types;
        struct{
        Voxel_t type;
        Voxel_t leftType;
        Voxel_t bottomType;
        Voxel_t frontType;
        }vx;
    }types;
    union{
        uint8_t faces;
        struct{
            uint8_t rightFace : 1; 
            uint8_t topFace : 1;
            uint8_t backFace : 1;
            uint8_t frontFace : 1;
            uint8_t bottomFace : 1;
            uint8_t leftFace : 1;
            uint8_t unused : 2;
        }face;
    } faces;
} VoxelMesh_t;
typedef void (*UpdateCB_t)(Vector3_32_t *id,uint16_t* locs, uint64_t* types,uint8_t* faces,uint16_t count);
typedef struct {
    Vector3_32_t pos;
    Vector3_32_t lastRenderedPos;
    Vector3_32_t lastPlayerPos;
    Vector3_32_t accessModifier;
    uint32_t rangeXZ;
    uint32_t rangeY;
    UpdateCB_t updateCB;
    Vector3_32_t* loadQueue;
    Vector3_32_t* renderBuffer;
    uint32_t queueHead;
    uint32_t renderHead;
    uint32_t renderMax;
    uint32_t renderWrite;
    uint32_t loadHead;
    uint32_t loadCount;
    uint32_t* chunks;
    Vector3_32_t* zeroQueue;
    uint32_t zeroCount;
    uint8_t queueDirty : 1;
} WorldView_t;
typedef bool (*CheckChunk_t)(WorldView_t* view,i32x4 pos);

#endif
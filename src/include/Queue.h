#ifndef QUEUE_H
#define QUEUE_H
#include <stdint.h>
#include <stdbool.h>
#include "Interface.h"
typedef uint32_t QueueElementSize_t;

typedef struct {
    bool (*enqueue)(void* queue,void* data,QueueElementSize_t elementSize);
    QueueElementSize_t (*dequeue)(void* queue,void* data);
    uint32_t (*count)(void* queue);
} QueueInterface_t;

CREATE_INTERFACE(Queue);

#endif
#ifndef VOXEL_H
#define VOXEL_H
typedef uint16_t vx_t;
typedef union{
    vx_t type;
    struct {
        vx_t unused : 12;
        vx_t item : 1;
        vx_t matterState : 2;
        vx_t nopaque : 1;
    } bitfield;
} Voxel_t;



#endif
#ifndef LINKED_LIST_H
#define LINKED_LIST_H
#include <stdbool.h>
#include <stdint.h>

typedef struct LL_Node{
    void* value;
    struct LL_Node* nextNode;
} LinkedListNode_t;

typedef struct {
    LinkedListNode_t* head;
} LinkedList_t;

typedef void (*LLForEach_t)(void *value);
typedef bool (*LLSearch_t)(void *value,void *searchData);

#ifdef __cplusplus
extern "C"{
#endif
void LL_add(LinkedList_t* list,void *value);
void *LL_remove(LinkedList_t* list,void *value);
void LL_forEach(LinkedListNode_t *head,LLForEach_t func);
LinkedListNode_t* LL_search(LinkedListNode_t *head,LLSearch_t search,void *searchData);
void LL_free(LinkedList_t* list);
uint32_t LL_len(LinkedList_t* list);
#ifdef __cplusplus
}
#endif
#endif
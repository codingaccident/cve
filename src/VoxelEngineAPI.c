#include "VoxelEngine.h"
#include "VoxelEngineAPI.h"
#include <stdlib.h>
#ifndef _W64
#include <pthread.h>
#endif
#include <stdbool.h>
#include <time.h>
#include <stdio.h>
#include <unistd.h>
#include "WorldView.h"
#include <omp.h>
#include <string.h>
#include "Vector3.h"
#include <bits/time.h>
#include "WorldGen.h"
// #include "test/TestSDL.h"
// #define OMP_NUM_THREADS=32


//all XYZ coords are in chunk space, i.e. worldspace / CHUNK_SIZE, which is currently 16, unless otherwise stated
// static VoxelEngineState state;
typedef struct timespec timespec;

typedef struct {
    ViewID_t viewID;
    uint32_t radXZ;
    uint32_t radY;
} ViewRangeUpdateEvent_t;

typedef struct {
    uint32_t count;
    ViewRangeUpdateEvent_t cache[256];
} VRUE_Cache_t;

static Metrics_t metrics;

// typedef struct {
//     unsigned int viewID;
//     Vector3_32_t newPos;
// } PosUpdateEvent_t;

// typedef struct {
//     uint32_t count;
//     PosUpdateEvent_t cache[2048];
// } PUE_Cache_t;

static VRUE_Cache_t cache ={
    .count = 0
};
// static PUE_Cache_t pCache = {
//     .count = 0
// };
static uint8_t run = 1;
static uint8_t done = 0;
// extern "C"{

#ifdef _WIN64
static void runMonitor(void* ignored);
#else
static void *runMonitor(void* threadShit);
#endif

void cve_Initialize(const char* dataDir,uint16_t slen){
    // state.dataDirLen = slen;
    // state.dataDir = (char*)malloc(sizeof(char) * slen);
    // memcpy(state.dataDir,dataDir,slen * sizeof(char));
    // printf("initialized with datadir: %s\n",state.dataDir);
    // state.world.capacity = 1;
    // state.world.chunks = (Chunk_t*) malloc(sizeof(Chunk_t) * state.world.capacity);
    // state.world.freeChunks = (uint32_t*) malloc(sizeof(uint32_t) * state.world.capacity);
    // state.views = (WorldView_t*) malloc(sizeof(WorldView_t) * 1);
    // state.viewCapacity = 1;
    // state.viewCount = 0;
    // state.run = 1;
    omp_set_num_threads(omp_get_num_procs());
    // omp_set_num_threads(4);
    initVoxelEngine(1234,dataDir,slen);
    #ifdef _WIN64
    _beginthread(&runMonitor,0,NULL);
    #else
    pthread_t monitorThread;
    pthread_create(&monitorThread,NULL,&runMonitor,NULL);
    #endif
}

Metrics_t DEBUG_GET_METRICS()
{
    return metrics;
}

static bool standardCheckChunkCallback(WorldView_t* view,i32x4 pos)
{
    Vector3_32_t id;
    id.nvec = pos;
    Chunk_t* chunk = getChunk(view,&id);
    if( //getViewChunkLoc(view,&id) != 0 && 
    // chunk->firstRender &&
     chunk->loaded && vEquals(&chunk->id,&id)) return false;
    // if(getViewChunkLoc(view,&id) != 0){
    //     printf("given loc: %i,%i,%i chunk at loc %i,%i,%i\n",x,y,z,chunk->id.x,chunk->id.y,chunk->id.z);
    //     printf("sccc states: loc: %i fr: %i ld: %i eq %i\n", getViewChunkLoc(view,&id),chunk->firstRender, chunk->loaded, vEquals(&chunk->id,&id) );
    // }
    return true;
}

static uint32_t elapsedTime(timespec start, timespec end)
{
    return ((start.tv_sec - end.tv_sec) * 1000000000) + ((start.tv_nsec - end.tv_nsec)/1000);
}

static void chunkWork(ViewID_t view)
{
    timespec start;
    clock_gettime(CLOCK_MONOTONIC,&start);
    loadChunks(view);
    timespec end;
    clock_gettime(CLOCK_MONOTONIC,&end);
    // printf("Gen execute time: %i us\n",micros.count());
    metrics.genTime = elapsedTime(start,end);
    metrics.loadHead = view->loadHead;
    clock_gettime(CLOCK_MONOTONIC,&start);
    renderChunks(view);
    metrics.renderHead = view->renderHead;
    clock_gettime(CLOCK_MONOTONIC,&end);
    // printf("Render execution time: %i us\n",micros.count());
    metrics.renderTime = elapsedTime(start,end);
    if(view->queueDirty){
        clock_gettime(CLOCK_MONOTONIC,&start);
        buildLoadQueue(view,standardCheckChunkCallback);
        clock_gettime(CLOCK_MONOTONIC,&end);
        metrics.queueTime = elapsedTime(start,end);
        metrics.queueSize = view->loadCount;
    }
}
#ifdef _WIN64
static void runMonitor(void* ignored)
#else
static void *runMonitor(void* threadShit)
#endif
{
    omp_set_num_threads(omp_get_num_procs());
    // sleep(20);
    while(run)
    {
        // printf("running monitor thread\n");
        // clock_t start = clock();
        // system("clear");

        // auto start = std::chrono::high_resolution_clock::now();
        timespec start;
        clock_gettime(CLOCK_MONOTONIC,&start);
        
        for(uint32_t i = 0; i < cache.count; i++)
        {
            updateViewRange(cache.cache[i].viewID,cache.cache[i].radXZ,cache.cache[i].radY);
        }
        cache.count = 0;
        // for(int i = 0; i < pCache.count;i++)
        // {
        //     getView(pCache.cache[i].viewID)->pos = pCache.cache[i].newPos;
        //     getView(pCache.cache[i].viewID)->queueDirty = 1;
        // }
        // pCache.count = 0;
        
        // for(int i = 0; i < getViewCount(); i++)
        // {
            
        //     loadChunks(getView(i));
        //     renderChunks(getView(i));
        //     if(getView(i)->queueDirty)
        //     buildLoadQueue(getView(i),*standardCheckChunkCallback);
        // }
        LL_forEach(getViewHead(),(LLForEach_t)chunkWork);
        timespec end;
        clock_gettime(CLOCK_MONOTONIC,&end);
        metrics.totalTime = elapsedTime(start,end);
        metrics.curentChunk = getWorldCurrent();
        metrics.totalChunk = getWorldCapacity();
        metrics.memSize = (uint32_t)(((double)((double)getWorldMemAlloc() * (double)sizeof(Chunk_t)) / 1000000.0));
        if(getViewHead())
        metrics.pos =((ViewID_t) getViewHead()->value)->pos;
        
        // printf("execution time took: %i us\n",micros.count());
        // printf("world memory size: %i chunks %f MB allocated: %f MB\n",getWorldCapacity(),((double)((double)getWorldCapacity() * (double)sizeof(Chunk_t)) / 1000000.0),((double)((double)getWorldMemAlloc() * (double)sizeof(Chunk_t)) / 1000000.0));
        // printf("world current chunk: %i available free: %i\n",getWorldCurrent(),availableFreeChunks());
        // if(getViewHead()){
        //     printf("view 0 position: %i,%i,%i\n",((ViewID_t)getViewHead()->value)->pos.coords.x,((ViewID_t)getViewHead()->value)->pos.coords.y,((ViewID_t)getViewHead()->value)->pos.coords.z);
        //     printf("queue size: %i load head: %i render head: %i\n",((ViewID_t)getViewHead()->value)->loadCount,((ViewID_t)getViewHead()->value)->loadHead,((ViewID_t)getViewHead()->value)->renderHead);
        // }
        
        // sleep(1);
        // usleep(30000);
    }
    done = 1;
    #ifndef _WIN64
    return threadShit;
    #endif
}

ViewID_t cve_CreateView(int radXZ, int radY, int x, int y, int z,UpdateCB_t updateCB){
    Vector3_32_t pos;
    pos.nvec = (i32x4){x,y,z,0};
    return createView(radXZ,radY,pos,updateCB);
}

void cve_UpdateViewRange(ViewID_t viewID,unsigned int radXZ,unsigned int radY){
    ViewRangeUpdateEvent_t *c = &cache.cache[cache.count++];
    c->viewID = viewID;
    c->radXZ = radXZ;
    c->radY = radY;
    
}

void cve_UpdateViewPosition(ViewID_t viewID,int x, int y, int z){
    // printf("Updating view postion to %i,%i,%i for view %p\n",x,y,z,viewID);
    viewID->pos.nvec = (i32x4){x,y,z,0};
    viewID->queueDirty = 1;
    viewID->queueHead = 0;
    // printf("updated\n");
    
}

static bool searchViewGet(ViewID_t view, Vector3_32_t* chunkSpace)
{
    Vector3_32_t local = vSub(chunkSpace,&view->pos);
    uint64_t sMag = sqrMag(&local);
    if(sMag > (view->rangeXZ * view->rangeXZ) && chunkSpace->coords.y < view->rangeY) return false;
    if(getChunk(view,chunkSpace)->loaded)
    return true;
    return false;
}
unsigned short cve_GetVoxel(int x, int y, int z){//SLOW FUNCTION, FINDS A VIEW THAT CAN SERVE THIS //worldspace
    Vector3_32_t chunkSpace;
    chunkSpace.nvec = (i32x4){x >> 4, y >> 4, z >> 4, 0};
    // for(int i = 0; i < getViewCount(); i++)
    // {
    //     Vector3_32_t local = vSub(&chunkSpace,&getView(i)->pos);
    //     uint64_t sMag = sqrMag(&local);
    //     if(sMag > (getView(i)->rangeXZ * getView(i)->rangeXZ) && chunkSpace.y < getView(i)->rangeY) continue;
    //     if(getChunk(getView(i),&chunkSpace)->loaded)
    //     return getChunk(getView(i),&chunkSpace)->voxels[x & 0xF][y & 0xF][z & 0xF].voxel.type;

    // }

    ViewID_t view = (ViewID_t)LL_search(getViewHead(),(LLSearch_t)searchViewGet,&chunkSpace);
    if(view)
    return getChunk(view,&chunkSpace)->voxels.voxels[x & 0xF][y & 0xF][z & 0xF].type;
    return 0xFFFF;
}
unsigned short cve_GetVoxelView(ViewID_t viewID, int x, int y, int z){//worldspace
    Vector3_32_t chunkSpace;
    chunkSpace.nvec = (i32x4){x >> 4, y >> 4, z >> 4, 0};
    return getChunk(viewID,&chunkSpace)->voxels.voxels[x & 0xF][y & 0xF][z & 0xF].type;
} 
typedef struct {
    uint16_t value;
    Vector3_32_t pos;
    int32_t x,y,z;
} SetEvent_t;
static bool searchViewSet(ViewID_t view,SetEvent_t* event)
{
    Vector3_32_t local = vSub(&event->pos,&view->pos);
    uint64_t sMag = sqrMag(&local);
    if(sMag > (view->rangeXZ * view->rangeXZ) && event->pos.coords.y < view->rangeY) return false;
    if(getChunk(view,&event->pos)->loaded){
        getChunk(view,&event->pos)->voxels.voxels[event->x & 0xF][event->y & 0xF][event->z & 0xF].type = event->value;
        saveChunk(view,&event->pos);
        return true;
    }
    return false;
}
void cve_SetVoxel(unsigned short value, int x, int y, int z){//SLOW FUNCTION, FINDS A VIEW THAT CAN SERVICE THIS // worldspace
    Vector3_32_t chunkSpace;
    chunkSpace.nvec = (i32x4){x >> 4, y >> 4, z >> 4, 0};
    SetEvent_t event = {
        .value = value,
        .pos = chunkSpace,
        .x = x,
        .y = y,
        .z = z
    };
    LL_search(getViewHead(),(LLSearch_t)searchViewSet,&event);
    // for(int i = 0; i < getViewCount(); i++)
    // {
    //     Vector3_32_t local = vSub(&chunkSpace,&getView(i)->pos);
    //     uint64_t sMag = sqrMag(&local);
    //     if(sMag > (getView(i)->rangeXZ * getView(i)->rangeXZ) && chunkSpace.y < getView(i)->rangeY) continue;
    //     if(getChunk(getView(i),&chunkSpace)->loaded){
    //         getChunk(getView(i),&chunkSpace)->voxels[x & 0xF][y & 0xF][z & 0xF].voxel.type = value;
    //         saveChunk(getView(i),&chunkSpace);
    //         return;
    //     }

    // }
    printf("attempted to set voxel at %i,%i,%i but no view was found that could service this location, or a view was found and the chunk is not loaded\n",x,y,z);
}
    
void cve_SetVoxelView(unsigned short value, ViewID_t viewID, int x, int y, int z){//worldspace
    Vector3_32_t chunkSpace;
    chunkSpace.nvec = (i32x4){x >> 4, y >> 4, z >> 4, 0};
    getChunk(viewID,&chunkSpace)->voxels.voxels[x & 0xF][y & 0xF][z & 0xF].type = value;
    saveChunk(viewID,&chunkSpace);

}
void cve_Shutdown(){
    run = false;
    while(!done){
        printf("waiting on shutdown...\n");
        sleep(1);
    }
    destroyVE();
    // for(int i = 0; i < state.viewCount; i++) destroyView(&state.views[i]);
    // free(state.world.chunks);
    // free(state.views);
    
}//free all resources

// }
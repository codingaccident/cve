SHELL = /bin/sh
CC=gcc
CXX=g++
SRCDIR		= ./src
BUILDDIR	= build
BINDIR 		= bin
DEBUG_FLAGS	= -g -fsanitize=undefined -Wall
CFLAGS		= -fPIC -I$(SRCDIR)/include -L./build_lib -Wl,-rpath=./ -O3 -fopenmp -mtune=znver1 -mavx2 -lgomp
CXXFLAGS	= -std=c++14 $(CFLAGS)

SRCXXEXT    = cpp
SRCEXT 		= c
DEPEXT      = d
OBJEXT      = o

SOURCES 	= $(shell find $(SRCDIR) -type f -regex "\.\/.+.c[pp]*")
HEADERS 	= $(shell echo $(SRCDIR)/include/*.h)
TOBJECTS 	= $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.$(OBJEXT)))
OBJECTS 	= $(TOBJECTS:.$(SRCXXEXT)=.$(OBJEXT))

LIB			= -L./bin -lVoxelEngine -lm -lFastNoiseSIMD -lSDL2 -lSDL2_ttf -ltcmalloc -lgomp -lvulkan
TARGET 		= $(BINDIR)/libVoxelEngine.so
LDFLAGS		= -shared -Wl,-rpath=./ -fPIC -Wall -lgomp
EXEC 		= $(BINDIR)/test
SHADERS 	= $(shell find ./test -type f -regex "\./\.\./test/.+\.[v|f][e|r][r|a][t|g]")
SHADER_FRAG = $(SHADERS:.frag=.spv)
SHADER_BIN 	= $(SHADER_FRAG:.vert=.spv)
TESTSRC		= ./test/Test.c ./test/TestSDL.cpp
RM			= rm -f
MKDIR		= mkdir

all 		: linux-test
linux 		: directories $(TARGET)
shaders 	: $(SHADER_BIN)
linux-test 	: linux shaders $(EXEC)
objects		: $(OBJECTS)

%.spv : %.frag
	glslc $< -o $@

%.spv : %.vert
	glslc $< -o $@

directories: 
	@mkdir -p $(BUILDDIR)
	@mkdir -p $(BINDIR)
	@cp build_lib/*.so bin/

$(BUILDDIR)/%.$(OBJEXT) : $(SRCDIR)/%.$(SRCEXT)
	@mkdir -p $(dir $@)
	$(CC) $(CFLAGS) -c -o $@ $<

$(BUILDDIR)/%.$(OBJEXT) : $(SRCDIR)/%.$(SRCXXEXT)
	@mkdir -p $(dir $@)
	$(CXX) $(CXXFLAGS) -c -o $@ $<
	
$(EXEC):
	$(CXX) $(CXXFLAGS) -o $(EXEC) $(TESTSRC) $(LIB)

$(TARGET): $(OBJECTS)
	$(CXX) ${LDFLAGS} -o $@ $^
	# -${RM} ${OBJECTS}

.PHONY: clean
clean:
	-${RM} ${TARGET} ${WINTARGET} ${OBJECTS} ${EXEC} ${WINEXEC} ${SHADER_BIN}
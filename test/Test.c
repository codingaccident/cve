#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "VoxelEngineAPI.h"
#include "Vector3.h"
#include <assert.h>
#include <unistd.h>
#ifdef _W64
#include <windows.h>
#endif
#include "TestSDL.h"
#include <chrono>

void testCB(Vector3_32_t *id,uint16_t *locs,uint64_t *types,uint8_t *faces,uint16_t count);
bool checkChunk(WorldView_t* view,uint32_t x,uint32_t y, uint32_t z);
static char textBuffer[33];

int main()
{


    
    // Vector3_32_t testv;
    // testv.nvec = (i32x4){1,2,3,4};
    // printf(" vec test: %i,%i,%i,%i\n",testv.coords.x,testv.coords.y,testv.coords.z,testv.coords.v);
    // return 0;
    //with this test we confirmed that [][][] notation compiles down to contiguous memory
    // Chunk_t chunk;

    // for(int x = 0; x < CHUNK_SIZE; x++)
    // for(int y = 0; y < CHUNK_SIZE; y++)
    // for(int z = 0; z < CHUNK_SIZE; z++)
    // {
    //     printf("%i,%i,%i : %p\n",x,y,z,&chunk.voxels[x][y][z]);
    // }
    // return 0;

    //  uint32_t *test = (uint32_t*) malloc(sizeof(uint32_t) * 21 * 11 * 21);

    // uint32_t index = 0;
    // WorldView_t dummy;
    // dummy.rangeXZ = 10;
    // dummy.rangeY = 5;
    
    // //what about 0,0,21 and 21,0,0 answer: they can't exist together, because 21 x implies lowest x is 1, and 21 z implies lowest z is 1

    // for(int x = 2; x < 23; x++)
    // // for(int y = -5; y < 6; y++)
    // for(int z = 2; z < 23; z++)
    // {
    //     Vector3_32_t testv = {
    //         .coords.x = x,
    //         .coords.y = 0,
    //         .coords.z = z
    //     };
    //     test[index++] = viewIndex(&dummy,&testv);
    //     printf("index for %i,%i,%i : %i\n",x,0,z,test[index-1]);
    // }

    // for(int i = 0; i < index; i++)
    // {
    //     uint32_t testIndex = test[i];
    //     for(int j = 0; j < index; j++)
    //     {
    //         if(j == i) continue;
    //         if(testIndex == test[j])
    //         {
    //             printf("collision found index: %i other index: %i value %i\n",i,j,testIndex);
    //         }
    //     }
    // }
    // return 0;
    // Chunk_t chunk;

    // printf("sizeof chunk.voxels: %i\n",sizeof(chunk.voxels));
    // CheckChunk_t ccCB = *checkChunk;
    // clock_t start = clock();
    // char* dataDir = "../test";

    // void *mem = (void*) malloc(4097 * sizeof(Vector3_32_t));
    // printf("ptr before align: %p\n",mem);
    // void *ptrHolder = mem;
    // // mem += 15;
    // // mem = (Vector3_32_t*)((uintptr_t)mem & ~ (uintptr_t)0x0f);
    // // printf("ptr after align:  %p\n",mem);
    // Vector3_32_t* testVecs = (Vector3_32_t*) mem;
    // for(int i = 0; i < 4096; i++)
    // {
    //     testVecs[i].nvec = (i32x4){0xf,0xf0,0xf00,0xF000};
    // }
    // Vector3_32_t test;
    // test.nvec = (i32x4){0xf,0xf0,0xf00,0xF000};
    // for(int i = 0; i < 4096; i++)
    // {
    //     assert(vEquals(&testVecs[i],&test));
    // }

    // free(ptrHolder);
    // FastNoiseSIMD *fastNoise = FastNoiseSIMD::NewFastNoiseSIMD(1234);
    // fastNoise->SetNoiseType(FastNoiseSIMD::NoiseType::Simplex);
    // float* set1 = FastNoiseSIMD::GetEmptySet(CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE);
    // float* set2 = FastNoiseSIMD::GetEmptySet(CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE);

    // for(int x = -50; x < 50; x++)
    // for(int y = -50; y < 50; y++)
    // for(int z = -50; z < 50; z++)
    // {
    //     fastNoise->FillNoiseSet(set1,x * CHUNK_SIZE,y * CHUNK_SIZE,z * CHUNK_SIZE,CHUNK_SIZE,CHUNK_SIZE,CHUNK_SIZE);
    //     fastNoise->FillNoiseSet(set2,x * CHUNK_SIZE,y * CHUNK_SIZE,z * CHUNK_SIZE,CHUNK_SIZE,CHUNK_SIZE,CHUNK_SIZE);
    //     printf("testing %i,%i,%i\n",x,y,z);
    //     for(int i = 0; i < 4096; i++)
    //     {
    //         if(set1[i] != set2[i]) {
    //             printf("mismatch between sets\n");
    //             assert(0);
    //         }
    //     }
    // }


    // return 0;


    InitializeTestWindow();


    while(!pollEvent())
    {
        drawFrame();
    }

//    return 0;

    #define TEST_VIEW_AMOUNT 1
    // sleep(30);
    // return 0;
    cve_Initialize("../test",strlen("../test"));
    // Vector3_32_t pos;
    // pos.nvec = (i32x4){0,0,0,0};

    uint64_t rad = 100;
    ViewID_t testviews[TEST_VIEW_AMOUNT];
    for(int i = 0; i < TEST_VIEW_AMOUNT; i++)
    {
        // sleep(10);
        testviews[i] = cve_CreateView(rad,10,rand()%800,rand()%200,rand()%200,testCB);
    }
    printf("views initialized\n");
    // sleep(10);
    // uint32_t testview2 = CreateView(rad,5,100,0,0,*testCB);
    // ChunkQueue_t queue;
    // generateQueue(&queue,ccCB,rad,pos);
    // uint32_t i = 0;
    // while(remainingElements(&queue) > 0)
    // {
    //     // printf("Chunk# %i\n",i++);
    //     Vector3_32_t id = dequeue(&queue);
    //     uint32_t chunkLoc = getFreeChunk();
    //     // printf("prepped chunk, creating\n");
    //     addChunk(&testview,&id,chunkLoc);

    //     Chunk_t* chunk = getChunk(&testview,&id);
    //     Voxel_t val;
    //     val.voxel.type = (id.coords.x < 0) ? 0xFF00 : 0x00FF;
        
    //     // printf("created chunk %i,%i,%i filling voxels\n",id.coords.x,id.coords.y,id.coords.z);
    //     fillVoxels(chunk->voxels,val);
        
    // }
    
    // printf("reading chunk 1,2,3 from view: \n");
    // Vector3_32_t testID = {
    //     .coords.x = 1,
    //     .coords.y = 2,
    //     .coords.z = 3,
    // };
    // Chunk_t* testChunk = getChunk(&testview,&testID);

    // printf("first block: %x\n",testChunk->voxels[0][0][0]);

    // printf("reading chunk -1,2,3 from view: \n");
    // testID.coords.x = -1;
    // testChunk = getChunk(&testview,&testID);

    // printf("first block: %x\n",testChunk->voxels[0][0][0]);

    // clock_t end = clock();
    // printf("execution time: %li ms\n",((end - start) * 1000)/CLOCKS_PER_SEC);

    // free(queue.data);
    int32_t x = 0,y = 0,z = 0;
    std::chrono::milliseconds elapsedms;
    std::chrono::time_point<std::chrono::system_clock> lastTime = std::chrono::high_resolution_clock::now();
    while(true){
        // #ifndef _W64
        // // sleep(5);
        // usleep(500000);
        // #else
        // Sleep(1000);
        // #endif
        // continue;

        std::chrono::time_point<std::chrono::system_clock> time = std::chrono::high_resolution_clock::now();
        elapsedms += std::chrono::duration_cast<std::chrono::milliseconds>(time-lastTime);
        lastTime = std::chrono::high_resolution_clock::now();
        
        if(elapsedms.count() > 50 * TEST_VIEW_AMOUNT)
        {
            elapsedms -= elapsedms;
            if(TEST_VIEW_AMOUNT > 1)
            {
                for(int i = 0; i < TEST_VIEW_AMOUNT; i++)
                {
                    
                    Vector3_32_t pos = testviews[i]->pos;
                    if(rand() % 2)
                    pos.coords.x++;
                    if(rand() % 2)
                    pos.coords.x--;
                    if(rand() % 2)
                    pos.coords.y++;
                    if(rand() % 2)
                    pos.coords.y--;
                    if(rand() % 2)
                    pos.coords.z++;
                    if(rand() % 2)
                    pos.coords.z--;
                    cve_UpdateViewPosition(testviews[i],pos.coords.x,pos.coords.y,pos.coords.z);//(i > 60) ? --z:++z);
                    
                }
            }else{
                    cve_UpdateViewPosition(testviews[0],++x,y,z);
            }
        }
        
        // if(x == 50)
        // {
        //     Vector3_32_t testChunk =
        //     {
        //         .coords.x = 50,
        //         .coords.y = 0,
        //         .coords.z = 0
        //     };
        //     Chunk_t* c0 = getChunk(getView(testview),&testChunk);
        //     Chunk_t* c1 = getChunk(getView(testview2),&testChunk);
            
        //     printf("view1 pos: %i,%i,%i view2 pos: %i,%i,%i\n",getView(testview)->pos.coords.x,getView(testview)->pos.coords.y,getView(testview)->pos.coords.z,getView(testview2)->pos.coords.x,getView(testview2)->pos.coords.y,getView(testview2)->pos.coords.z);
        //     printf("chunk loc view 1: %i view 2: %i\n",getViewChunkLoc(getView(testview),&testChunk),getViewChunkLoc(getView(testview2),&testChunk));
        //     printf("chunk id at loc 0: %i,%i,%i at loc 1: %i,%i,%i\n",c0->id.coords.x,c0->id.coords.y,c0->id.coords.z,c1->id.coords.x,c1->id.coords.y,c1->id.coords.z);
        //     assert(getChunk(getView(testview),&testChunk) == getChunk(getView(testview2),&testChunk));
        // }
        ViewID_t view0 = (ViewID_t) getViewHead()->value;
        clearScreen();
        const int boxSize = 300/rad;
        for(int x = (int)(view0->pos.coords.x - view0->rangeXZ); x <= (int)(view0->pos.coords.x + view0->rangeXZ); x++)
        {
            // printf("x: %i",x);
            for(int z = (int)(view0->pos.coords.z - view0->rangeXZ); z <= (int)(view0->pos.coords.z + view0->rangeXZ); z++)
            {
                Vector3_32_t id;
                id.nvec = (i32x4){x,view0->pos.coords.y,z,0};
                
                // Vector3_32_t local = vSub(&id,&view0->pos);
                if(vEquals(&view0->pos,&id))
                {
                    // printf("\033[0;34m");
                    drawBox(0x10,0x30,0xFF,(x - view0->pos.coords.x + view0->rangeXZ)*boxSize,(z - view0->pos.coords.z+ view0->rangeXZ)*boxSize,boxSize);
                }
                else if(!getChunk(view0,&id)->loaded || getViewChunkLoc(view0,&id) == 0)
                {
                    // printf("\033[0;31m");
                    drawBox(0xFF,0x00,0x00,(x - view0->pos.coords.x+ view0->rangeXZ)*boxSize,(z - view0->pos.coords.z+ view0->rangeXZ)*boxSize,boxSize);
                }
                else if(getChunk(view0,&id)->firstRender)
                {
                    // printf("\033[0;32m");
                    drawBox(0x00,0xFF,0x00,(x - view0->pos.coords.x+ view0->rangeXZ)*boxSize,(z - view0->pos.coords.z+ view0->rangeXZ)*boxSize,boxSize);
                }
                else
                {
                    // printf("\033[0;33m");
                    drawBox(0xFF,0xFF,0x0,(x - view0->pos.coords.x+ view0->rangeXZ)*boxSize,(z - view0->pos.coords.z+ view0->rangeXZ)*boxSize,boxSize);
                }
                // printf("\u2588\u2588\033[0m");
                // if(getChunk(view0,&id)->loaded)
                // if(!vEquals(&getChunk(view0,&id)->id,&id))
                // {
                //     Vector3_32_t* testID = &getChunk(view0,&id)->id;
                //     // printf("weird chunk state at %i,%i,%i id we got: %i,%i,%i\n",id.coords.x,id.coords.y,id.coords.z,testID->x, testID->y,testID->z);
                //     if(!vEquals(&getChunk(view0,testID)->id,testID))
                //     {
                //         printf("chunk at loc: %i id: %i,%i,%i requestedID: %i,%i,%i\n",getViewChunkLoc(view0,&id),testID->coords.x,testID->coords.y,testID->coords.z,id.coords.x,id.coords.y,id.coords.z);
                //         printf("memory corruption detected\n");
                //         assert(0);
                //     }
                // }
                
            }
            // printf("<-xz:xy->");
            for(int y = (int)(view0->pos.coords.y - view0->rangeY); y <= (int)(view0->pos.coords.y + view0->rangeY); y++)
            {
                Vector3_32_t id;
                id.nvec = (i32x4){x,y,view0->pos.coords.z,0};
                // Vector3_32_t local = vSub(&id,&view0->pos);
                if(vEquals(&view0->pos,&id))
                {
                    // printf("\033[0;34m");
                    drawBox(0x10,0x30,0xFF,((x - view0->pos.coords.x + view0->rangeXZ)*boxSize) + ((view0->rangeXZ * 2) + 2) * boxSize ,(y - view0->pos.coords.y+ view0->rangeY)*boxSize,boxSize);
                }
                else if(!getChunk(view0,&id)->loaded || getViewChunkLoc(view0,&id) == 0)
                {
                    // printf("\033[0;31m");
                    drawBox(0xFF,0x00,0x00,((x - view0->pos.coords.x+ view0->rangeXZ)*boxSize) + ((view0->rangeXZ * 2) + 2) * boxSize ,(y - view0->pos.coords.y+ view0->rangeY)*boxSize,boxSize);
                }
                else if(getChunk(view0,&id)->firstRender)
                {
                    // printf("\033[0;32m");
                    drawBox(0x00,0xFF,0x00,((x - view0->pos.coords.x+ view0->rangeXZ)*boxSize) + ((view0->rangeXZ * 2) + 2) * boxSize ,(y - view0->pos.coords.y+ view0->rangeY)*boxSize,boxSize);
                }
                else
                {
                    // printf("\033[0;33m");
                    drawBox(0xFF,0xFF,0x0,((x - view0->pos.coords.x+ view0->rangeXZ)*boxSize) + ((view0->rangeXZ * 2) + 2) * boxSize ,(y - view0->pos.coords.y+ view0->rangeY)*boxSize,boxSize);
                }
                // printf("\u2588\u2588\033[0m");
                // printf("\uFBBD\uFBBD\033[0m");

            }
            
            // printf("\n");
        }
        
        // printf("\n");
        for(int y = (int)(view0->pos.coords.y - view0->rangeY); y <= (int)(view0->pos.coords.y + view0->rangeY); y++)
        {
            // printf("x: %i",x);
            for(int z = (int)(view0->pos.coords.z - view0->rangeXZ); z <= (int)(view0->pos.coords.z + view0->rangeXZ); z++)
            {
                Vector3_32_t id;
                id.nvec = (i32x4){view0->pos.coords.x,y,z,0};
                // Vector3_32_t local = vSub(&id,&view0->pos);
                if(vEquals(&view0->pos,&id))
                {
                    // printf("\033[0;34m");
                    drawBox(0x10,0x30,0xFF,((y - view0->pos.coords.y + view0->rangeY)*boxSize) , (((view0->rangeXZ * 2) + 2) * boxSize) +(z - view0->pos.coords.z+ view0->rangeXZ)*boxSize,boxSize);
                }
                else if(!getChunk(view0,&id)->loaded || getViewChunkLoc(view0,&id) == 0)
                {
                    // printf("\033[0;31m");
                    drawBox(0xFF,0x00,0x00,((y - view0->pos.coords.y+ view0->rangeY)*boxSize) , (((view0->rangeXZ * 2) + 2) * boxSize) +(z - view0->pos.coords.z+ view0->rangeXZ)*boxSize,boxSize);
                }
                else if(getChunk(view0,&id)->firstRender)
                {
                    // printf("\033[0;32m");
                    drawBox(0x00,0xFF,0x00,((y - view0->pos.coords.y+ view0->rangeY)*boxSize) , (((view0->rangeXZ * 2) + 2) * boxSize) +(z - view0->pos.coords.z+ view0->rangeXZ)*boxSize,boxSize);
                }
                else
                {
                    // printf("\033[0;33m");
                    drawBox(0xFF,0xFF,0x0,((y - view0->pos.coords.y+ view0->rangeY)*boxSize) , (((view0->rangeXZ * 2) + 2) * boxSize) +(z - view0->pos.coords.z+ view0->rangeXZ)*boxSize,boxSize);
                }
                // printf("\u2588\u2588\033[0m");

            }
            // printf("<-yz:yx->");
            for(int x = (int)(view0->pos.coords.x - view0->rangeXZ); x <= (int)(view0->pos.coords.x + view0->rangeXZ); x++)
            {
                Vector3_32_t id;
                id.nvec = (i32x4){x,y,view0->pos.coords.z,0};
                // Vector3_32_t local = vSub(&id,&view0->pos);
                if(vEquals(&view0->pos,&id))
                {
                    // printf("\033[0;34m");
                // else if(sqrMag(&local) > view0->range * view0->range)
                //     printf("\033[0;35m");
                }
                else if(getViewChunkLoc(view0,&id) == 0)
                {
                    // printf("\033[0;31m");
                }
                else if(getChunk(view0,&id)->firstRender)
                {
                    // printf("\033[0;32m");
                }
                else
                {
                    // printf("\033[0;33m");
                }
                // printf("\u2588\u2588\033[0m");

            }
            
            // printf("\n");
        }
        Vector3_32_t id = view0->pos;
        Chunk_t* chunk000 = getChunk(view0,&id);
        id.coords.x++;
        Chunk_t* chunk001 = getChunk(view0,&id);
        id.coords.z++;
        Chunk_t* chunk011 = getChunk(view0,&id);
        id.coords.x--;
        Chunk_t* chunk010 = getChunk(view0,&id);
        id.coords.x--;
        Chunk_t* chunk110 = getChunk(view0,&id);
        id.coords.z--;
        Chunk_t* chunk100 = getChunk(view0,&id);
        id.coords.z--;
        Chunk_t* chunk1n0 = getChunk(view0,&id);
        id.coords.x++;
        Chunk_t* chunk0n0 = getChunk(view0,&id);
        id.coords.x++;
        Chunk_t* chunk0n1 = getChunk(view0,&id);
        const uint32_t blockSize = 25;
        const uint32_t xoffset = 2000;
        
        for(int x = 0; x < 16; x++)
        for(int z = 0; z < 16; z++)
        {
            if(chunk000->loaded)
            if(chunk000->voxels.voxels[x][0][z].type != 0) drawBox(0x7F,0x7F,0x7F,(xoffset + (blockSize * 16)) + (x * blockSize),(blockSize * 16) + (z * blockSize),blockSize);
            if(chunk001->loaded)
            if(chunk001->voxels.voxels[x][0][z].type != 0) drawBox(0x7F,0x7F,0xFF,(xoffset + (blockSize * 32)) + (x * blockSize),(blockSize * 16) + (z * blockSize),blockSize);
            if(chunk011->loaded)
            if(chunk011->voxels.voxels[x][0][z].type != 0) drawBox(0x7F,0xFF,0xFF,(xoffset + (blockSize * 32)) + (x * blockSize),(blockSize * 32) + (z * blockSize),blockSize);
            if(chunk010->loaded)
            if(chunk010->voxels.voxels[x][0][z].type != 0) drawBox(0x7F,0xFF,0x7F,(xoffset + (blockSize * 16)) + (x * blockSize),(blockSize * 32) + (z * blockSize),blockSize);
            if(chunk110->loaded)
            if(chunk110->voxels.voxels[x][0][z].type != 0) drawBox(0xFF,0xFF,0x7F,(xoffset + (blockSize * 0)) + (x * blockSize),(blockSize * 32) + (z * blockSize),blockSize);
            if(chunk100->loaded)
            if(chunk100->voxels.voxels[x][0][z].type != 0) drawBox(0xFF,0x7F,0x7F,(xoffset + (blockSize * 0)) + (x * blockSize),(blockSize * 16) + (z * blockSize),blockSize);
            if(chunk1n0->loaded)
            if(chunk1n0->voxels.voxels[x][0][z].type != 0) drawBox(0xFF,0x00,0x7F,(xoffset + (blockSize * 0)) + (x * blockSize),(blockSize * 0) + (z * blockSize),blockSize);
            if(chunk0n0->loaded)
            if(chunk0n0->voxels.voxels[x][0][z].type != 0) drawBox(0x7F,0x00,0x7F,(xoffset + (blockSize * 16)) + (x * blockSize),(blockSize * 0) + (z * blockSize),blockSize);
            if(chunk0n1->loaded)
            if(chunk0n1->voxels.voxels[x][0][z].type != 0) drawBox(0x00,0x7F,0xFF,(xoffset + (blockSize * 32)) + (x * blockSize),(blockSize * 0) + (z * blockSize),blockSize);
        }
        
        uint32_t textY = 1200;
        sprintf(textBuffer,   "generation time: %i us",DEBUG_GET_METRICS().genTime);
        drawText(textBuffer,300,textY,strlen(textBuffer)*50,75);
        sprintf(textBuffer,"render time:     %i us",DEBUG_GET_METRICS().renderTime);
        textY+=75;
        drawText(textBuffer,300,textY,strlen(textBuffer)*50,75);
        sprintf(textBuffer, "queue time:      %i us",DEBUG_GET_METRICS().queueTime);
        textY+=75;
        drawText(textBuffer,300,textY,strlen(textBuffer)*50,75);
        sprintf(textBuffer, "total time:      %i us",DEBUG_GET_METRICS().totalTime);
        textY+=75;
        drawText(textBuffer,300,textY,strlen(textBuffer)*50,75);
        sprintf(textBuffer, "render head:     %i",DEBUG_GET_METRICS().renderHead);
        textY+=75;
        drawText(textBuffer,300,textY,strlen(textBuffer)*50,75);
        sprintf(textBuffer, "load head:       %i",DEBUG_GET_METRICS().loadHead);
        textY+=75;
        drawText(textBuffer,300,textY,strlen(textBuffer)*50,75);
        sprintf(textBuffer, "queue size:      %i",DEBUG_GET_METRICS().queueSize);
        textY+=75;
        drawText(textBuffer,300,textY,strlen(textBuffer)*50,75);
        sprintf(textBuffer, "memory alloc:    %i mb",DEBUG_GET_METRICS().memSize);
        textY+=75;
        drawText(textBuffer,300,textY,strlen(textBuffer)*50,75);
        sprintf(textBuffer, "current chunk:   %i",DEBUG_GET_METRICS().curentChunk);
        textY+=75;
        drawText(textBuffer,300,textY,strlen(textBuffer)*50,75);
        sprintf(textBuffer, "max chunks:      %i",DEBUG_GET_METRICS().totalChunk);
        textY+=75;
        drawText(textBuffer,300,textY,strlen(textBuffer)*50,75);
        sprintf(textBuffer, "view position:   %i,%i,%i",DEBUG_GET_METRICS().pos.coords.x,DEBUG_GET_METRICS().pos.coords.y,DEBUG_GET_METRICS().pos.coords.z);
        textY+=75;
        drawText(textBuffer,300,textY,strlen(textBuffer)*50,75);
        
        
        
        updateWindow();
        
        if(pollEvent()) break;
        usleep(16000);
    }//spin for now

    cve_Shutdown();
}

void testCB(Vector3_32_t *id,uint16_t *locs,uint64_t *types,uint8_t *faces,uint16_t count)
{
    // if(id->x == 0 && id->y == 0 && id->z == 0)
    // printf("First value in voxelMesh count: %i loc: %x type: %x\n",count,mesh[0].location,mesh[0].type);
    // free(mesh);
}

bool checkChunk(WorldView_t* view,uint32_t x,uint32_t y, uint32_t z)
{
    return true;
}
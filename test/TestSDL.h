#ifndef TESTSDL_H
#define TESTSDL_H
#include "../rendering/Vector.h"
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_FORCE_DEFAULT_ALIGNED_GENTYPES
#include <glm/glm.hpp>
typedef struct {
    __attribute__((aligned(16))) glm::mat4 model;
    __attribute__((aligned(16))) glm::mat4 view;
    __attribute__((aligned(16))) glm::mat4 proj;
} UniformBufferObject_t;
void InitializeTestWindow();
void drawBox(uint8_t r, uint8_t g, uint8_t b, uint32_t xStart, uint32_t yStart, uint32_t size);
void updateWindow();
int pollEvent();
void clearScreen();
void drawText(const char *text,uint32_t xStart,uint32_t yStart,uint32_t xSize,uint32_t ySize);
void drawFrame();
#endif
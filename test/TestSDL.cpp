#include <SDL2/SDL.h>
#include "TestSDL.h"
#include <stdint.h>
#include <unistd.h>
#include <assert.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_vulkan.h>
#include <vulkan/vulkan.h>
#include "Set.h"
#include <math.h>
#include "../rendering/Vertex.h"
#include <glm/gtc/matrix_transform.hpp>
#include <time.h>
#define VMA_STATIC_VULKAN_FUNCTIONS 0
#define VMA_DYNAMIC_VULKAN_FUNCTIONS 1
#define VMA_IMPLEMENTATION
#include <vk_mem_alloc.h>
#include "../rendering/BMP.h"
#define TINYOBJLOADER_IMPLEMENTATION
#include <tiny_obj_loader.h>
#include "List.h"
LIST(Vertex3_t)
LIST(uint16_t)
#define MAX_FRAMES_IN_FLIGHT 2

static SDL_Window *window;
static SDL_Renderer *renderer;
static VkSurfaceKHR vulkan_surface;
static VkInstance instance;
static TTF_Font *messageFont;
static VkPhysicalDevice device = VK_NULL_HANDLE;
static VkDevice logicalDevice = VK_NULL_HANDLE;
static VkQueue graphicsQueue;
static VkQueue presentQueue;
static float queuePriority = 1.0f;
const char *requiredExtensions[] = {VK_KHR_SWAPCHAIN_EXTENSION_NAME};
const uint32_t reqExtensionsLen = 1;
static VkSwapchainKHR swapchain;
static VkFormat scFormat;
static VkExtent2D scExtent;
static uint32_t scImageCount = 0;
static VkImage *scimages = NULL;
static VkImageView *scImageViews = NULL;
static VkRenderPass renderPass;
static VkPipelineLayout pipelineLayout;
static VkDescriptorSetLayout setLayout;
static VkPipeline graphicsPipeline;
static VkFramebuffer *framebuffers = NULL;
static VkCommandPool commandPool;
static VkCommandPool transientCPool;
static VkCommandBuffer commandBuffer[MAX_FRAMES_IN_FLIGHT];
static VkSemaphore imageAvailableSem[MAX_FRAMES_IN_FLIGHT];
static VkSemaphore renderFinishedSem[MAX_FRAMES_IN_FLIGHT];
static VkFence inFlightFence[MAX_FRAMES_IN_FLIGHT];
static bool resize = false;
// static const uint32_t testVertsSize = 10;
// static const Vertex3_t testVerts[testVertsSize] = {
//     {{-0.5f,-0.5f ,0.0f},  {1.0f, 0.0f,0.0f},   {1.0f, 0.0f}},
//     {{0.5f, -0.5f ,0.0f},  {0.0f, 1.0f, 0.0f},  {0.0f, 0.0f}},
//     {{-0.5f, 0.5f ,0.0f},  {0.0f, 0.0f, 1.0f},  {1.0f, 1.0f}},
//     {{0.5f,  0.5f ,0.0f},  {1.0f, 1.0f, 1.0f},  {0.0f, 1.0f}},
//     {{-0.5f,-0.5f ,-0.5f},  {1.0f, 0.0f, 1.0f}, {1.0f, 0.0f}},
//     {{0.5f, -0.5f ,-0.5f},  {0.0f, 1.0f, 1.0f}, {0.0f, 0.0f}},
//     {{-0.5f,-0.5f ,-0.5f},  {1.0f, 0.0f,0.0f},   {1.0f, 0.0f}},
//     {{0.5f, -0.5f ,-0.5f},  {0.0f, 1.0f, 0.0f},  {0.0f, 0.0f}},
//     {{-0.5f, 0.5f ,-0.5f},  {0.0f, 0.0f, 1.0f},  {1.0f, 1.0f}},
//     {{0.5f,  0.5f ,-0.5f},  {1.0f, 1.0f, 1.0f},  {0.0f, 1.0f}},
// };
// static const uint32_t testIndicesSize = 18;
// static const uint16_t testindices[testIndicesSize] = {
//     0,1,3, 2,0,3, 0,4,5, 5,1,0, 6,7,9, 8,6,9
// };

Vertex3_t_List_t verts;
uint16_t_List_t indices;

static VkBuffer vertexBuffer;
static VmaAllocation vBufferMemory;
static VkBuffer indexBuffer;
static VmaAllocation indexMemory;
static VkBuffer uniformBuffers[MAX_FRAMES_IN_FLIGHT];
static VmaAllocation uBuffersMemory[MAX_FRAMES_IN_FLIGHT];
static VkDescriptorPool descriptorPool;
static VkDescriptorSet descriptorSets[MAX_FRAMES_IN_FLIGHT];
static VkImage dash;
static VmaAllocation dashMem;
static VkImageView dashView;
static VkSampler dashSampler;
static VkImage depthImage;
static VmaAllocation depthMem;
static VkImageView depthView;

static timespec prevTime;
static uint32_t currentFrame = 0;

static VmaAllocator allocator;

int shutDown(void *userdata,SDL_Event *event);
typedef struct {
    VkSurfaceCapabilitiesKHR capabilities;
    VkSurfaceFormatKHR* formats;
    VkPresentModeKHR* presentModes;
} SwapChainSupport_t;
static void selectDevice();

typedef struct {
    uint32_t graphicsQueue;
    uint32_t presentQueue;
    bool valid;
} QueueIndices_t;

static void createLogicalDevice(QueueIndices_t *indices);
static void getQueues(VkPhysicalDevice device,QueueIndices_t *indices);
static void createSwapChain();
static void createImageViews();
static void createGraphicsPipeline();
static void createRenderPass();
static void createFrameBuffers();
static void createCommandPool();
static void createCommandBuffers();
static void createSyncObjects();
static void cleanupSwapChain();
static void createVertexBuffer();
static void createIndexBuffer();
static void createUniformBuffers();
static void createDescriptorSetLayout();
static void createDescriptorPool();
static void createDescriptorSets();
static void createBuffer(VkDeviceSize size,VkBufferUsageFlags flags,VmaAllocationCreateFlagBits allocFlags, VkMemoryPropertyFlags props, VkBuffer *buffer,VmaAllocation *bufferMem);
static void createDash();
static VkCommandBuffer beginSingleCommand();
static void endSingleCommand(VkCommandBuffer buffer);
static void createDashView();
static void createDashSampler();
static void createDepthResources();
static VkFormat findDepthFormat();
static void loadDashModel();

#define VULKAN_TEST

void InitializeTestWindow()
{
    int result = SDL_Init(SDL_INIT_VIDEO);
    printf("setup SDL result: %i\n",result);
    window = SDL_CreateWindow("CVE Test Visualization",0,0,3840,2160,SDL_WINDOW_VULKAN | SDL_WINDOW_RESIZABLE);
    uint32_t pCount = 0;
    SDL_Vulkan_GetInstanceExtensions(window,&pCount,NULL);
    #ifndef _W64
    const char* extensionNames[pCount];
    #else
    const char** extensionNames = (char**)malloc(4096);
    #endif
    SDL_Vulkan_GetInstanceExtensions(window,&pCount,extensionNames);
    VkApplicationInfo appInfo {};
    
    appInfo.pApplicationName = "CVE Test Visualization Vulkan";
    appInfo.pEngineName = "No Engine";
    appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.applicationVersion = VK_MAKE_VERSION(1,0,0);
    appInfo.apiVersion = VK_API_VERSION_1_2;
    appInfo.pNext = NULL;
    // printf("extension len: %i\n",pCount);
    // sleep(2);
    for(uint32_t i = 0; i < pCount; i++)
    printf("loading with extension: %s\n",extensionNames[i]);
    const char* validationLayers[] = {"VK_LAYER_KHRONOS_validation"};
    VkInstanceCreateInfo info = {
        .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
        .pApplicationInfo = &appInfo,
        .enabledLayerCount = 1,
        .ppEnabledLayerNames = validationLayers,
        .enabledExtensionCount = pCount,
        .ppEnabledExtensionNames = extensionNames
    };

    VkResult res;
   
    res = vkCreateInstance(&info,NULL,&instance);

    if(res != VK_SUCCESS)
    {
        printf("failed to setup Vulkan, error code: %i\n",res);
        assert(0);
    }

    if(!SDL_Vulkan_CreateSurface(window,instance,&vulkan_surface))
    {
        printf("failed to create vulkan surface, error: %s\n",SDL_GetError());
        assert(0);
    }
    #ifdef VULKAN_TEST

    

    Vertex3_t_listInit(&verts,1000);
    uint16_t_listInit(&indices,1000);
    selectDevice();
    QueueIndices_t indices;
    printf("getting queue indices\n");
    getQueues(device,&indices);
    printf("queue indices: graphics: %i present: %i\n",indices.graphicsQueue,indices.presentQueue);
    createLogicalDevice(&indices);
    printf("setting up VMA\n");
    VmaVulkanFunctions vulkanFunctions = {};
    vulkanFunctions.vkGetInstanceProcAddr = &vkGetInstanceProcAddr;
    vulkanFunctions.vkGetDeviceProcAddr = &vkGetDeviceProcAddr;

    VmaAllocatorCreateInfo allocatorInfo = {};
    allocatorInfo.vulkanApiVersion = VK_API_VERSION_1_2;
    allocatorInfo.physicalDevice = device;
    allocatorInfo.device = logicalDevice;
    allocatorInfo.instance = instance;
    allocatorInfo.pVulkanFunctions = &vulkanFunctions;

    vmaCreateAllocator(&allocatorInfo,&allocator);
    
    printf("getting device graphics queue\n");
    vkGetDeviceQueue(logicalDevice,indices.graphicsQueue,0,&graphicsQueue);
    vkGetDeviceQueue(logicalDevice,indices.presentQueue,0,&presentQueue);
    printf("creating swapchain\n");
    createSwapChain();
    printf("creating image views\n");
    createImageViews();
    printf("creating render pass\n");
    createRenderPass();
    printf("creating descriptor set layout\n");
    createDescriptorSetLayout();
    printf("creating graphics pipeline\n");
    createGraphicsPipeline();
    printf("creating depth resources");
    createDepthResources();
    printf("creating framebuffers\n");
    createFrameBuffers();
    printf("creating command pool\n");
    createCommandPool();
    printf("loading Dash\n");
    createDash();
    printf("creating Dash view\n");
    createDashView();
    printf("creating Dash sampler\n");
    createDashSampler();
    printf("loading Dash model\n");
    loadDashModel();
    printf("creating vertex buffer\n");
    createVertexBuffer();
    printf("creating index buffer\n");
    createIndexBuffer();
    printf("creating uniform buffer\n");
    createUniformBuffers();
    printf("creating descriptor pool\n");
    createDescriptorPool();
    printf("creating descriptor sets\n");
    createDescriptorSets();
    printf("creating command buffer\n");
    createCommandBuffers();
    printf("creating synchronization structures\n");
    createSyncObjects();
    clock_gettime(CLOCK_MONOTONIC,&prevTime);
    #else

    renderer = SDL_CreateRenderer(window,-1,0);
    TTF_Init();
    messageFont = TTF_OpenFont("../test/data-unifon.ttf",24);
    if(window == NULL)
    {
        printf("failed, error: %s\n",SDL_GetError());
    }else{
        printf("created window\n");
    }
    printf("pixel format: %s\n",SDL_GetPixelFormatName(SDL_GetWindowPixelFormat(window)));
    SDL_PixelFormat *format = SDL_AllocFormat(SDL_GetWindowPixelFormat(window));
    printf("bytes per pixel: %i\n",format->BytesPerPixel);
    
    SDL_AddEventWatch(*shutDown,NULL);
    #endif
}
typedef struct {
    VkDeviceQueueCreateInfo* queues;
    uint32_t index;
} DQCI_IndexPacked_t;

static void querySwapChainSupport(SwapChainSupport_t* support,VkPhysicalDevice device)
{
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device,vulkan_surface,&support->capabilities);
    uint32_t formatCount;
    vkGetPhysicalDeviceSurfaceFormatsKHR(device,vulkan_surface,&formatCount,NULL);
    if(formatCount){
        support->formats = (VkSurfaceFormatKHR*) calloc(formatCount + 1,sizeof(VkSurfaceFormatKHR));
        vkGetPhysicalDeviceSurfaceFormatsKHR(device,vulkan_surface,&formatCount,support->formats);
    }
    uint32_t presentModeCount;
    vkGetPhysicalDeviceSurfacePresentModesKHR(device,vulkan_surface,&presentModeCount,NULL);
    if(presentModeCount)
    {
        support->presentModes = (VkPresentModeKHR*)calloc(presentModeCount + 1,sizeof(VkPresentModeKHR));
        vkGetPhysicalDeviceSurfacePresentModesKHR(device,vulkan_surface,&presentModeCount,support->presentModes);
    }
}

static bool buildCreateQueueInfo(int32_t* data,DQCI_IndexPacked_t* queues)//abusing LL_search here
{
    VkDeviceQueueCreateInfo qCreateInfo;
    qCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    qCreateInfo.queueFamilyIndex = *data;
    qCreateInfo.queueCount = 1;
    qCreateInfo.pQueuePriorities = &queuePriority;
    queues->queues[queues->index++] = qCreateInfo;
    return false;
}
static void createLogicalDevice(QueueIndices_t* indices)
{
    printf("creating logical device\n");
    Uint32_Set_t set {};
    Uint32_set_add(&set,&indices->graphicsQueue);
    Uint32_set_add(&set,&indices->presentQueue);
    printf("building set for queues, len: %i\n",LL_len(&set.data));
    VkDeviceQueueCreateInfo qCreateInfos[LL_len(&set.data)];
    DQCI_IndexPacked_t qCreateInfosPacked = {
        .queues = qCreateInfos,
        .index = 0
    };
    printf("pushing set to array\n");
    LL_search(set.data.head,(LLSearch_t)buildCreateQueueInfo,&qCreateInfosPacked);
    printf("building device info\n");
    VkPhysicalDeviceFeatures logFeatures {};
    logFeatures.samplerAnisotropy = VK_TRUE;
    VkDeviceCreateInfo createInfo {};
    createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    createInfo.pEnabledFeatures = &logFeatures;
    createInfo.queueCreateInfoCount = qCreateInfosPacked.index;
    createInfo.pQueueCreateInfos = qCreateInfos;
    createInfo.enabledExtensionCount = reqExtensionsLen;
    createInfo.ppEnabledExtensionNames = requiredExtensions;
    printf("creating device\n");
    vkCreateDevice(device,&createInfo,NULL,&logicalDevice);
    LL_free(&set.data);

}

static void getQueues(VkPhysicalDevice device,QueueIndices_t* indices)
{
    uint32_t count;
    vkGetPhysicalDeviceQueueFamilyProperties(device,&count,NULL);
    VkQueueFamilyProperties props[count];
    vkGetPhysicalDeviceQueueFamilyProperties(device,&count,props);
    indices->graphicsQueue = -1;
    indices->presentQueue = -1;
    int32_t i = 0;
    for(VkQueueFamilyProperties prop : props)
    {
        if(prop.queueFlags & VK_QUEUE_GRAPHICS_BIT)
        {
            indices->graphicsQueue = i;
        }
        VkBool32 psupport = false;
        vkGetPhysicalDeviceSurfaceSupportKHR(device,i,vulkan_surface,&psupport);//check if vulkan surface should be initialized and used here, might be horse before the cart
        if(psupport)
        {
            indices->presentQueue = i;
            
        }
        if(indices->presentQueue != -1 && indices->graphicsQueue != -1){ 
            indices->valid = true;
            break;
        }

        i++;
    }
}
static bool checkExtensionSupport(VkPhysicalDevice device)
{
    uint32_t extensionsCount;
    vkEnumerateDeviceExtensionProperties(device,NULL,&extensionsCount,NULL);
    VkExtensionProperties props[extensionsCount];
    vkEnumerateDeviceExtensionProperties(device,NULL,&extensionsCount,props);
    char** extensionsCheck = (char**)malloc(sizeof(requiredExtensions));
    memcpy(extensionsCheck,requiredExtensions,sizeof(requiredExtensions));
    for(VkExtensionProperties prop:props)
    {
        // printf("checking extension: %s\n",prop.extensionName);
        for(int i = 0; i < reqExtensionsLen;i++){
            if(!extensionsCheck[i]) continue;
            if(strcmp(prop.extensionName,extensionsCheck[i]))
            {
                extensionsCheck[i] = NULL;
                break;
            }
            i++;
        }
    }
    bool ret = true;
    for(int i = 0; i < reqExtensionsLen;i++)
        if(extensionsCheck[i]) ret = false;
    free(extensionsCheck);
    return ret;
}
static VkSurfaceFormatKHR selectSurfaceFormat(SwapChainSupport_t *scs)
{
    VkSurfaceFormatKHR* format = scs->formats;
    while(format)
    {
        if(format->format == VK_FORMAT_B8G8R8A8_SRGB && format->colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) return *format;
        format = &scs->formats[1];
    }
    return scs->formats[0];
}
static VkPresentModeKHR selectPresentMode(SwapChainSupport_t *scs)
{
    VkPresentModeKHR* presentMode = scs->presentModes;
    while(presentMode)
    {
        if(*presentMode == VK_PRESENT_MODE_MAILBOX_KHR) return *presentMode;
        presentMode = &scs->presentModes[1];
    }
    return VK_PRESENT_MODE_FIFO_KHR;
}
static VkExtent2D selectSwapExtent(const VkSurfaceCapabilitiesKHR capabilities)
{
    if(capabilities.currentExtent.width != UINT32_MAX)
    {
        // printf("returning current extent %i, %i\n",capabilities.currentExtent.height, capabilities.currentExtent.width);
        return capabilities.currentExtent;
    }
    int w,h;
    SDL_GetWindowSize(window,&w,&h);
    VkExtent2D ret {
        .width = (uint32_t)w,
        .height = (uint32_t)h
    };
    SDL_clamp(ret.width,capabilities.minImageExtent.width,capabilities.maxImageExtent.width);
    SDL_clamp(ret.height,capabilities.minImageExtent.height,capabilities.maxImageExtent.height);
    return ret;
}
static void selectDevice()
{
    uint32_t devCount;
    vkEnumeratePhysicalDevices(instance,&devCount,NULL);
    printf("Physical vulkan devices: %i\n",devCount);
    VkPhysicalDevice devices[devCount];
    vkEnumeratePhysicalDevices(instance,&devCount,devices);
    for(int i = 0; i < devCount; i++)
    {
        VkPhysicalDeviceProperties props;
        VkPhysicalDeviceFeatures features;
        vkGetPhysicalDeviceProperties(devices[i],&props);
        vkGetPhysicalDeviceFeatures(devices[i],&features);
        if(props.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU && features.geometryShader){
                    
            QueueIndices_t indices;
            getQueues(devices[i],&indices);
            // printf("got queues checking ext support and queue valid\n");
            SwapChainSupport_t scs{};
            querySwapChainSupport(&scs,devices[i]);
            if(checkExtensionSupport(devices[0]) && indices.valid && scs.formats && scs.presentModes){
                device = devices[i];
                
                printf("selecting device: %s\n",props.deviceName);
                free(scs.formats);
                free(scs.presentModes);
                return;
            }
        }
    }
}

static void createSwapChain(){
    SwapChainSupport_t scs {};
    querySwapChainSupport(&scs,device);
    VkSurfaceFormatKHR format = selectSurfaceFormat(&scs);//remember to free, need better ergos on this
    VkPresentModeKHR presentMode = selectPresentMode(&scs);
    VkExtent2D extent = selectSwapExtent(scs.capabilities);
    printf("extent: %i, %i\n",extent.height,extent.width);
    uint32_t imageCount = scs.capabilities.minImageCount + 1;
    if(scs.capabilities.maxImageCount > 0 && imageCount > scs.capabilities.maxImageCount)
    {
        imageCount = scs.capabilities.maxImageCount;
    }
    VkSwapchainCreateInfoKHR scCreateInfo {};
    scCreateInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    scCreateInfo.surface = vulkan_surface;
    scCreateInfo.minImageCount = imageCount;
    scCreateInfo.imageFormat = format.format;
    scCreateInfo.imageColorSpace = format.colorSpace;
    scCreateInfo.imageArrayLayers = 1;
    scCreateInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    scCreateInfo.imageExtent = extent;
    // printf("not dead1\n");
    QueueIndices_t qi;
    getQueues(device,&qi);
    uint32_t qFamIndices[] = {qi.graphicsQueue,qi.presentQueue};
    if(qi.graphicsQueue != qi.presentQueue)
    {
        scCreateInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
        scCreateInfo.queueFamilyIndexCount = 2;
        scCreateInfo.pQueueFamilyIndices = qFamIndices;
    }else{
        scCreateInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        scCreateInfo.queueFamilyIndexCount = 0;
        scCreateInfo.pQueueFamilyIndices = NULL;
    }
    scCreateInfo.preTransform = scs.capabilities.currentTransform;//can use this to flip the image to give us positve Y up
    scCreateInfo.presentMode = presentMode;
    scCreateInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    scCreateInfo.clipped = VK_TRUE;
    scCreateInfo.oldSwapchain = VK_NULL_HANDLE;
    // printf("not dead2\n");
    if(vkCreateSwapchainKHR(logicalDevice,&scCreateInfo,NULL,&swapchain) != VK_SUCCESS)
    {
        printf("failed to create swapchain\n");
        assert(0);
    }
    // printf("not dead3\n");
    free(scs.formats);
    free(scs.presentModes);
    vkGetSwapchainImagesKHR(logicalDevice,swapchain,&imageCount,NULL);
    if(scimages) free(scimages);
    scimages = (VkImage*)malloc(sizeof(VkImage) * imageCount);
    vkGetSwapchainImagesKHR(logicalDevice,swapchain,&imageCount,scimages);
    scImageCount = imageCount;
    scFormat = format.format;
    scExtent = extent;
}

static void createImageViews()
{
    if(scImageViews) free(scImageViews);
    scImageViews = (VkImageView*) malloc(sizeof(VkImageView) * scImageCount);
    for(int i = 0; i < scImageCount; i++)
    {
        VkImageViewCreateInfo createInfo {};
        createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        createInfo.image = scimages[i];
        createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
        createInfo.format = scFormat;
        createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        createInfo.subresourceRange.baseMipLevel = 0;
        createInfo.subresourceRange.levelCount = 1;
        createInfo.subresourceRange.baseArrayLayer = 0;
        createInfo.subresourceRange.layerCount = 1;
        if(vkCreateImageView(logicalDevice,&createInfo,NULL,&scImageViews[i]) != VK_SUCCESS)
        {
            printf("failed to create image view: %i\n",i);
            assert(0);
        }
    }
}

static char* readVulkanShader(uint32_t* len, const char* fileName)
{
    FILE* file = fopen(fileName,"rb");
    fseek(file,0L,SEEK_END);
    uint32_t size = ftell(file);
    rewind(file);
    char* shader = (char*)malloc(size);
    *len = size;
    fread(shader,1,size,file);
    fclose(file);
    return shader;
}

static VkShaderModule createShaderModule(char *bytecode, uint32_t len)
{
    VkShaderModuleCreateInfo createInfo {};
    createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    createInfo.codeSize = len;
    createInfo.pCode = (uint32_t*)bytecode;
    VkShaderModule shaderModule;
    if(vkCreateShaderModule(logicalDevice,&createInfo,NULL,&shaderModule) != VK_SUCCESS)
    {
        printf("failed to create shader module\n");
        assert(0);
    }
    return shaderModule;
}

static void createRenderPass()
{
    VkAttachmentDescription colorAttachment {};
    colorAttachment.format = scFormat;
    colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
    colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

    VkAttachmentDescription depthAttachment {};
    depthAttachment.format = findDepthFormat();
    depthAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
    depthAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    depthAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    depthAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    depthAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    depthAttachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    VkAttachmentReference caRef {};
    caRef.attachment = 0;
    caRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkAttachmentReference depthRef {};
    depthRef.attachment = 1;
    depthRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    VkSubpassDescription subpass {};
    subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &caRef;
    subpass.pDepthStencilAttachment = &depthRef;

    VkSubpassDependency dep{};
    dep.srcSubpass = VK_SUBPASS_EXTERNAL;
    dep.dstSubpass = 0;
    dep.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
    dep.srcAccessMask = 0;
    dep.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
    dep.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

    VkAttachmentDescription descs[2] = {colorAttachment,depthAttachment};

    VkRenderPassCreateInfo rpi {};
    rpi.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    rpi.attachmentCount = 2;
    rpi.pAttachments = descs;
    rpi.subpassCount = 1;
    rpi.pSubpasses = &subpass;
    rpi.dependencyCount = 1;
    rpi.pDependencies = &dep;

    if(vkCreateRenderPass(logicalDevice,&rpi,NULL,&renderPass) != VK_SUCCESS)
    {
        printf("unable to create render pass\n");
        assert(0);
    }


}

static void createGraphicsPipeline()
{
    uint32_t len;
    char* shader = readVulkanShader(&len,"../test/FragmentShaderTest.spv");
    VkShaderModule fragShader = createShaderModule(shader,len);
    free(shader);
    shader = readVulkanShader(&len,"../test/VertexShaderTest.spv");
    VkShaderModule vertShader = createShaderModule(shader,len);
    free(shader);

    VkPipelineShaderStageCreateInfo vertCreateInfo {};
    vertCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    vertCreateInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
    vertCreateInfo.module = vertShader;
    vertCreateInfo.pName = "main";


    VkPipelineShaderStageCreateInfo fragCreateInfo {};
    fragCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    fragCreateInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
    fragCreateInfo.module = fragShader;
    fragCreateInfo.pName = "main";

    VkPipelineShaderStageCreateInfo shaderStages[] = {vertCreateInfo,fragCreateInfo};

    VkPipelineVertexInputStateCreateInfo viCreateInfo {};
    viCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    viCreateInfo.vertexBindingDescriptionCount = 1;
    VkVertexInputBindingDescription binDesc = vert3_getBindingDescription();
    viCreateInfo.pVertexBindingDescriptions = &binDesc;
    viCreateInfo.vertexAttributeDescriptionCount = 3;
    VkVertexInputAttributeDescription *attDesc = vert3_getAttributeDescription();
    viCreateInfo.pVertexAttributeDescriptions = attDesc;

    VkPipelineInputAssemblyStateCreateInfo iaCreateInfo {};
    iaCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    iaCreateInfo.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    iaCreateInfo.primitiveRestartEnable = VK_FALSE;

    VkViewport viewport {};
    viewport.x = 0.0f;
    viewport.y = 0.0f;
    viewport.width = (float) scExtent.width;
    viewport.height = (float) scExtent.height;
    viewport.minDepth = 0.0f;
    viewport.maxDepth = 1.0f;

    VkRect2D scissor{};
    scissor.offset = {0,0};
    scissor.extent = scExtent;

    VkPipelineViewportStateCreateInfo pvsCreateInfo {};
    pvsCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    pvsCreateInfo.viewportCount = 1;
    pvsCreateInfo.pViewports = &viewport;
    pvsCreateInfo.scissorCount = 1;
    pvsCreateInfo.pScissors = &scissor;

    VkPipelineRasterizationStateCreateInfo prsCreateInfo {};
    prsCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    prsCreateInfo.depthClampEnable = VK_FALSE;
    prsCreateInfo.rasterizerDiscardEnable = VK_FALSE;
    prsCreateInfo.polygonMode = VK_POLYGON_MODE_FILL;
    prsCreateInfo.lineWidth = 1.0f;
    prsCreateInfo.cullMode = VK_CULL_MODE_BACK_BIT;
    prsCreateInfo.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
    prsCreateInfo.depthBiasEnable = VK_FALSE;
    prsCreateInfo.depthBiasConstantFactor = 0.0f;
    prsCreateInfo.depthBiasClamp = 0.0f;
    prsCreateInfo.depthBiasSlopeFactor = 0.0f;
    VkPipelineMultisampleStateCreateInfo msCreateInfo {};
    msCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    msCreateInfo.sampleShadingEnable = VK_FALSE;
    msCreateInfo.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
    msCreateInfo.minSampleShading = 1.0f;
    msCreateInfo.pSampleMask = NULL;
    msCreateInfo.alphaToCoverageEnable = VK_FALSE;
    msCreateInfo.alphaToOneEnable = VK_FALSE;

    VkPipelineColorBlendAttachmentState cba {};
    cba.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
    cba.blendEnable = VK_FALSE;
    cba.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
    cba.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
    cba.colorBlendOp = VK_BLEND_OP_ADD;
    cba.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
    cba.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
    cba.alphaBlendOp = VK_BLEND_OP_ADD;

    VkPipelineColorBlendStateCreateInfo cb {};
    cb.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    cb.logicOpEnable = VK_FALSE;
    cb.logicOp = VK_LOGIC_OP_COPY;
    cb.attachmentCount = 1;
    cb.pAttachments = &cba;
    cb.blendConstants[0] = 0.0f;
    cb.blendConstants[1] = 0.0f;
    cb.blendConstants[2] = 0.0f;
    cb.blendConstants[3] = 0.0f;

    VkPipelineLayoutCreateInfo plci {};
    plci.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    plci.setLayoutCount = 1;
    plci.pSetLayouts = &setLayout;
    plci.pushConstantRangeCount = 0;
    plci.pPushConstantRanges = NULL;
    if(vkCreatePipelineLayout(logicalDevice,&plci,NULL,&pipelineLayout) != VK_SUCCESS)
    {
        printf("failed to create pipeline layout\n");
        assert(0);
    }

    VkPipelineDepthStencilStateCreateInfo dssi {};
    dssi.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    dssi.depthTestEnable = VK_TRUE;
    dssi.depthWriteEnable = VK_TRUE;
    dssi.depthCompareOp = VK_COMPARE_OP_LESS;
    dssi.depthBoundsTestEnable = VK_FALSE;
    dssi.minDepthBounds = 0.0f;
    dssi.maxDepthBounds = 1.0f;
    dssi.stencilTestEnable = VK_FALSE;
    dssi.front = {};
    dssi.back = {};

    VkGraphicsPipelineCreateInfo gpi {};
    gpi.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    gpi.stageCount = 2;
    gpi.pStages = shaderStages;
    gpi.pVertexInputState = &viCreateInfo;
    gpi.pInputAssemblyState = &iaCreateInfo;
    gpi.pViewportState = &pvsCreateInfo;
    gpi.pRasterizationState = &prsCreateInfo;
    gpi.pMultisampleState = &msCreateInfo;
    gpi.pDepthStencilState = NULL;
    gpi.pColorBlendState = &cb;
    gpi.pDynamicState = NULL;
    gpi.layout = pipelineLayout;
    gpi.renderPass = renderPass;
    gpi.subpass = 0;
    gpi.basePipelineHandle = NULL;
    gpi.basePipelineIndex = -1;
    gpi.pDepthStencilState = &dssi;


    if(vkCreateGraphicsPipelines(logicalDevice,VK_NULL_HANDLE,1,&gpi,NULL,&graphicsPipeline) != VK_SUCCESS)
    {
        printf("failed to create graphics pipeline\n");
        assert(0);
    }


    vkDestroyShaderModule(logicalDevice,fragShader,NULL);
    vkDestroyShaderModule(logicalDevice,vertShader,NULL);
    
}

static void createDescriptorSetLayout(){
    VkDescriptorSetLayoutBinding binding {};
    binding.binding = 0;
    binding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    binding.descriptorCount = 1;
    binding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
    binding.pImmutableSamplers = NULL;

    VkDescriptorSetLayoutBinding dashbinding {};
    dashbinding.binding = 1;
    dashbinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    dashbinding.descriptorCount = 1;
    dashbinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
    dashbinding.pImmutableSamplers = NULL;
    VkDescriptorSetLayoutBinding bindings[2] = {
        binding,dashbinding
    };

    VkDescriptorSetLayoutCreateInfo info {};
    info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    info.bindingCount = 2;
    info.pBindings = bindings;

    if(vkCreateDescriptorSetLayout(logicalDevice,&info,NULL,&setLayout) != VK_SUCCESS)
    {
        printf("unable to create descriptor set layout\n");
        assert(0);
    }

}

static void createFrameBuffers()
{
    if(framebuffers) free(framebuffers);
    framebuffers = (VkFramebuffer*)malloc(scImageCount * sizeof(VkFramebuffer));

    for(int i = 0; i < scImageCount; i++)
    {
        VkImageView attachments[] = {
            scImageViews[i],
            depthView
        };
        VkFramebufferCreateInfo fbi {};
        fbi.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        fbi.renderPass = renderPass;
        fbi.attachmentCount = 2;
        fbi.pAttachments = attachments;
        fbi.width = scExtent.width;
        fbi.height = scExtent.height;
        fbi.layers = 1;
        if(vkCreateFramebuffer(logicalDevice,&fbi,NULL,&framebuffers[i]) != VK_SUCCESS)
        {
            printf("failed to create framebuffer %i\n",i);
            assert(0);
        }


    }
}

static void createCommandPool()
{
    QueueIndices_t indices;
    getQueues(device,&indices);

    VkCommandPoolCreateInfo pci {};
    pci.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    pci.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
    pci.queueFamilyIndex = indices.graphicsQueue;

    if(vkCreateCommandPool(logicalDevice,&pci,NULL,&commandPool) != VK_SUCCESS)
    {
        printf("failed to create command pool\n");
        assert(0);
    }
    
    pci.flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT;

    if(vkCreateCommandPool(logicalDevice,&pci,NULL,&transientCPool) != VK_SUCCESS)
    {
        printf("failed to create transient command pool\n");
        assert(0);
    }
}

static void createUniformBuffers()
{
    VkDeviceSize bufferSize = sizeof(UniformBufferObject_t);
    for(int i = 0; i < MAX_FRAMES_IN_FLIGHT; i++)
    {
        createBuffer(bufferSize,VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,VMA_ALLOCATION_CREATE_HOST_ACCESS_RANDOM_BIT,VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,&uniformBuffers[i],&uBuffersMemory[i]);
    }
}

static void createCommandBuffers()
{
    VkCommandBufferAllocateInfo alloc{};
    alloc.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    alloc.commandPool = commandPool;
    alloc.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    alloc.commandBufferCount = 1;
    alloc.commandBufferCount = MAX_FRAMES_IN_FLIGHT;
    if(vkAllocateCommandBuffers(logicalDevice,&alloc,commandBuffer) != VK_SUCCESS)
    {
        printf("failed to create command buffer\n");
        assert(0);
    }
}

static void recordCommandBuffer(VkCommandBuffer buffer,uint32_t imageIndex)
{
    VkCommandBufferBeginInfo begin{};
    begin.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    begin.flags = 0;
    begin.pInheritanceInfo = NULL;

    if(vkBeginCommandBuffer(buffer,&begin) != VK_SUCCESS)
    {
        printf("failed to start command buffer\n");
        assert(0);
    }

    VkRenderPassBeginInfo rpBegin {};
    rpBegin.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    rpBegin.renderPass = renderPass;
    rpBegin.framebuffer = framebuffers[imageIndex];
    rpBegin.renderArea.offset = {0,0};
    rpBegin.renderArea.extent = scExtent;
    VkClearValue clearColor = {{{0.0f, 0.0f, 0.0f, 1.0f}}};
    VkClearValue clearDepth = {1.0f,0};
    VkClearValue clearValues[2] = {clearColor, clearDepth};
    rpBegin.clearValueCount = 2;
    rpBegin.pClearValues = clearValues;

    vkCmdBeginRenderPass(buffer,&rpBegin,VK_SUBPASS_CONTENTS_INLINE);
    vkCmdBindPipeline(buffer,VK_PIPELINE_BIND_POINT_GRAPHICS,graphicsPipeline);
    VkBuffer vertBuffers[] = {vertexBuffer};
    VkDeviceSize offsets[] = {0};
    vkCmdBindVertexBuffers(buffer,0,1,vertBuffers,offsets);
    vkCmdBindIndexBuffer(buffer,indexBuffer,0,VK_INDEX_TYPE_UINT16);

    vkCmdBindDescriptorSets(buffer,VK_PIPELINE_BIND_POINT_GRAPHICS,pipelineLayout,0,1,&descriptorSets[currentFrame],0,NULL);

    vkCmdDrawIndexed(buffer,indices.count,1,0,0,0);
    vkCmdEndRenderPass(buffer);
    if(vkEndCommandBuffer(buffer) != VK_SUCCESS)
    {
        printf("failed ending command buffer\n");
        assert(0);
    }


}

static uint32_t findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags props)
{
    
    VkPhysicalDeviceMemoryProperties memProps;
    vkGetPhysicalDeviceMemoryProperties(device,&memProps);
    for(uint32_t i = 0; i <= memProps.memoryTypeCount ;i++)
    {
        if(typeFilter & (1 << i) && (memProps.memoryTypes[i].propertyFlags & props) == props)
        {
            return i;
        }
    }
    printf("unable to find suitable memory type for vertex buffer\n");
    assert(0);
}
static void createBuffer(VkDeviceSize size,VkBufferUsageFlags flags,VmaAllocationCreateFlagBits allocFlags, VkMemoryPropertyFlags props, VkBuffer *buffer,VmaAllocation *bufferMem)
{
    VkBufferCreateInfo info {};
    info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    info.size = size;
    info.usage = flags;
    info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    VmaAllocationCreateInfo allocInfo {};
    allocInfo.usage = VMA_MEMORY_USAGE_AUTO;
    allocInfo.requiredFlags = props;
    // allocInfo.memoryTypeBits = props;
    allocInfo.flags = allocFlags;
    // allocInfo.flags = flags;
    // allocation.
    VkResult res = vmaCreateBuffer(allocator,&info,&allocInfo,buffer,bufferMem,NULL);
    if(res != VK_SUCCESS)
    {
        printf("failed to create buffer, error code: %i\n",res);
    }
    // if(vkCreateBuffer(logicalDevice,&info,NULL,buffer) != VK_SUCCESS)
    // {
    //     printf("failed to create buffer\n");
    //     assert(0);
    // }

    // VkMemoryRequirements memReq;
    // vkGetBufferMemoryRequirements(logicalDevice,*buffer,&memReq);
    // VkMemoryAllocateInfo allocInfo {};
    // allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    // allocInfo.allocationSize = memReq.size;
    // allocInfo.memoryTypeIndex = findMemoryType(memReq.memoryTypeBits,VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

    // if(vkAllocateMemory(logicalDevice,&allocInfo,NULL,bufferMem) != VK_SUCCESS)
    // {
    //     printf("failed to allocate memory for buffer\n");
    //     assert(0);
    // }

    // vkBindBufferMemory(logicalDevice,*buffer,*bufferMem,0);
}

static void createImage(uint32_t width, uint32_t height, VkFormat format,VkImageTiling tiling, VkImageUsageFlags flags,VkMemoryPropertyFlags props, VkImage *image, VmaAllocation *imageMem)
{
    VkImageCreateInfo info {};
    info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    info.imageType = VK_IMAGE_TYPE_2D;
    info.extent.width = width;
    info.extent.height = height;
    info.extent.depth = 1;
    info.mipLevels = 1;
    info.arrayLayers = 1;
    info.format = format;
    info.tiling = tiling;
    info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    info.usage = flags;
    info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    info.samples = VK_SAMPLE_COUNT_1_BIT;
    info.flags = 0;

    VmaAllocationCreateInfo allocInfo {};
    allocInfo.usage = VMA_MEMORY_USAGE_AUTO;
    allocInfo.requiredFlags = props;
    // allocInfo.flags = VMA_ALLOCATION_CREATE_HOST_ACCESS_RANDOM_BIT;

    printf("w: %i h: %i \n",info.extent.width,info.extent.height);
    VkResult res = vmaCreateImage(allocator,&info,&allocInfo,image,imageMem,NULL);
    if(res != VK_SUCCESS)
    {
        printf("failed to create image, res: %i\n",res);
        assert(0);
    }
}
static VkImageView createImageView(VkImage image, VkFormat format, VkImageAspectFlags aspectFlags)
{
    VkImageViewCreateInfo createInfo {};
    createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    createInfo.image = image;
    createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
    
    createInfo.format = format;
    createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
    createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
    createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
    createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
    createInfo.subresourceRange.aspectMask = aspectFlags;
    createInfo.subresourceRange.baseMipLevel = 0;
    createInfo.subresourceRange.levelCount = 1;
    createInfo.subresourceRange.baseArrayLayer = 0;
    createInfo.subresourceRange.layerCount = 1;
    VkImageView ret;
    if(vkCreateImageView(logicalDevice,&createInfo,NULL,&ret) != VK_SUCCESS)
    {
        printf("failed to create image view\n");
        assert(0);
    }
    return ret;
}

static void transitionImageLayout(VkImage image, VkFormat format, VkImageLayout oLayout, VkImageLayout nLayout)
{
    VkCommandBuffer tbuffer = beginSingleCommand();

    VkImageMemoryBarrier barrier{};
    barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    barrier.oldLayout = oLayout;
    barrier.newLayout = nLayout;
    barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.image = image;
    barrier.subresourceRange = {
        .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
        .baseMipLevel = 0,
        .levelCount = VK_REMAINING_MIP_LEVELS,
        .baseArrayLayer = 0,
        .layerCount = 1
    };
    VkPipelineStageFlags srcStage;
    VkPipelineStageFlags dstStage;
    if(oLayout == VK_IMAGE_LAYOUT_UNDEFINED && nLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
    {
        barrier.srcAccessMask = 0;
        barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        srcStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        dstStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
    } else if(oLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && nLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
    {
        barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
        srcStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
        dstStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
    }
    vkCmdPipelineBarrier(tbuffer,srcStage,dstStage,0,0,NULL,0,NULL,1,&barrier);

    endSingleCommand(tbuffer);
}

static void copyBufferToImage(VkBuffer buffer, VkImage image, uint32_t width, uint32_t height)
{
    VkCommandBuffer tbuffer = beginSingleCommand();

    VkBufferImageCopy copy {};
    copy.bufferOffset = 0;
    copy.bufferRowLength = 0; // may need to use this to specify our padding in BMPs, or extract BMPs to tightly packed pixels
    copy.bufferImageHeight = 0;
    copy.imageSubresource = {
        .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
        .mipLevel = 0,
        .baseArrayLayer = 0,
        .layerCount = 1
    };
    copy.imageOffset = {0,0,0};
    copy.imageExtent = {
        width,
        height,
        1
    };

    vkCmdCopyBufferToImage(tbuffer,buffer,image,VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,1,&copy);

    endSingleCommand(tbuffer);
}

static void createDash()
{
    BitmapHeader_t bmpHeader;
    DIBHeader_t dibHeader;
    char* pixels;
    uint8_t error = bmp_loadFile("../test/dash_hair.bmp",&bmpHeader,&dibHeader,&pixels);
    if(error)
    {
        printf("failed to load dash, error code: %i\n",error);
        assert(0);
    }

    if(!pixels)
    {
        printf("dash pixels null\n");
        assert(0);
    }
    printf("image w: %i h: %i\n",dibHeader.width, dibHeader.height);
    uint32_t width = abs(dibHeader.width);
    uint32_t height = abs(dibHeader.height);

    VkBuffer stagingBuffer;
    VmaAllocation stagingMem;
    createBuffer(dibHeader.imageSize,VK_BUFFER_USAGE_TRANSFER_SRC_BIT,VMA_ALLOCATION_CREATE_HOST_ACCESS_RANDOM_BIT,VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,&stagingBuffer,&stagingMem);

    void* rawDash;
    vmaMapMemory(allocator,stagingMem,&rawDash);
    memcpy(rawDash,pixels,dibHeader.imageSize);
    vmaUnmapMemory(allocator,stagingMem);
    free(pixels);

    createImage(width,height,VK_FORMAT_B8G8R8A8_SRGB,VK_IMAGE_TILING_OPTIMAL,VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,&dash,&dashMem);
    if(!dash)
    {
        printf("dash null\n");
    }

    transitionImageLayout(dash,VK_FORMAT_B8G8R8A8_SRGB, VK_IMAGE_LAYOUT_UNDEFINED,VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
    copyBufferToImage(stagingBuffer,dash,width,height);
    transitionImageLayout(dash,VK_FORMAT_B8G8R8A8_SRGB,VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

}

static void createDashView()
{
    dashView = createImageView(dash,VK_FORMAT_B8G8R8A8_SRGB,VK_IMAGE_ASPECT_COLOR_BIT);

    // VkImageViewCreateInfo createInfo {};
    // createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    // createInfo.image = dash;
    // createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
    
    // createInfo.format = VK_FORMAT_B8G8R8A8_SRGB;
    // createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
    // createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
    // createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
    // createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
    // createInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    // createInfo.subresourceRange.baseMipLevel = 0;
    // createInfo.subresourceRange.levelCount = 1;
    // createInfo.subresourceRange.baseArrayLayer = 0;
    // createInfo.subresourceRange.layerCount = 1;
    // if(vkCreateImageView(logicalDevice,&createInfo,NULL,&dashView) != VK_SUCCESS)
    // {
    //     printf("failed to create dash view\n");
    //     assert(0);
    // }
      
}

static void createDashSampler()
{
    VkSamplerCreateInfo info {};
    info.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    info.magFilter = VK_FILTER_LINEAR;
    info.minFilter = VK_FILTER_LINEAR;
    info.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    info.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    info.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    info.anisotropyEnable = VK_TRUE;
    VkPhysicalDeviceProperties props {};
    vkGetPhysicalDeviceProperties(device,&props);
    info.maxAnisotropy = props.limits.maxSamplerAnisotropy;
    info.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
    info.unnormalizedCoordinates = VK_FALSE;
    info.compareEnable = VK_FALSE;
    info.compareOp = VK_COMPARE_OP_ALWAYS;
    info.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
    info.mipLodBias = 0.0f;
    info.minLod = 0.0f;
    info.maxLod = 0.0f;
    if(vkCreateSampler(logicalDevice,&info,NULL,&dashSampler) != VK_SUCCESS)
    {
        printf("failed to create Dash sampler\n");
        assert(0);
    }
}

static void loadDashModel()
{
    tinyobj::attrib_t attrib;
    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> materials;
    std::string warn, err;

    if(!tinyobj::LoadObj(&attrib,&shapes,&materials,&warn,&err,"../test/Rainbowdash.obj","../test/"))
    {
        printf("unable to load Dash model\n");
        printf(warn.c_str());
        printf(err.c_str());
        assert(0);
    }
    printf("materials size: %i\n",materials.size());
    for(const auto material :materials)
    {
        printf("material : %s\n",material.name.c_str());
        printf("diffuse texname : %s\n",material.diffuse_texname.c_str());
        printf("specular texname : %s\n",material.specular_texname.c_str());
        printf("ambient texname : %s\n",material.ambient_texname.c_str());
        printf("alpha texname : %s\n",material.alpha_texname.c_str());
        printf("displacement texname : %s\n",material.displacement_texname.c_str());
        printf("reflection texname : %s\n",material.reflection_texname.c_str());
    }
    for(const auto& shape : shapes)
    {
        printf("Shape: %s\n",shape.name.c_str());
        printf("first material: %i\n",shape.mesh.material_ids[0]);
        for(const auto& index :shape.mesh.indices)
        {
            Vertex3_t vertex{
                .pos = {
                    attrib.vertices[3 * index.vertex_index + 0],
                    attrib.vertices[3 * index.vertex_index + 1],
                    attrib.vertices[3 * index.vertex_index + 2]
                },
                .color = {1.0f,1.0f,1.0f},
                .texCoord = {
                    attrib.texcoords[2 * index.texcoord_index + 0],
                    attrib.texcoords[2 * index.texcoord_index + 1],
                },
                
            };
            

            if((vertex.texCoord[0] < 0.0f || vertex.texCoord[0] > 1.0f) || (vertex.texCoord[1] < 0.0f || vertex.texCoord[1] > 1.0f))
            {
                printf("weird vertex texcoord: %f %f\n",vertex.texCoord[0],vertex.texCoord[1]);
            }


            Vertex3_t_listAdd(&verts,vertex,0);
            uint16_t_listAdd(&indices,indices.count,0);
        }
    }

}

static VkFormat findSupportedFormat(VkFormat *candidates, VkImageTiling tiling,VkFormatFeatureFlags features)
{
    while(*candidates)
    {
        VkFormatProperties props;
        VkFormat format = *candidates;
        vkGetPhysicalDeviceFormatProperties(device,format,&props);

        if(tiling == VK_IMAGE_TILING_LINEAR && (props.linearTilingFeatures & features) == features)
        {
            return format;
        } else if(tiling == VK_IMAGE_TILING_OPTIMAL && (props.optimalTilingFeatures & features) == features)
        {
            return format;
        }

        candidates += sizeof(VkFormat);
    }
    printf("unable to find suitable format\n");
    assert(0);
}

static VkFormat findDepthFormat()
{
    VkFormat candidates[3] = {VK_FORMAT_D32_SFLOAT,VK_FORMAT_D32_SFLOAT_S8_UINT,VK_FORMAT_D24_UNORM_S8_UINT};
    return findSupportedFormat(candidates,VK_IMAGE_TILING_OPTIMAL,VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT);
}

static bool hasStencilComponent(VkFormat format)
{
    return format == VK_FORMAT_D32_SFLOAT_S8_UINT || format == VK_FORMAT_D24_UNORM_S8_UINT;
}

static void createDepthResources()
{
    VkFormat depthFormat = findDepthFormat();
    createImage(scExtent.width,scExtent.height,depthFormat,VK_IMAGE_TILING_OPTIMAL,VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,&depthImage,&depthMem);
    depthView = createImageView(depthImage,depthFormat,VK_IMAGE_ASPECT_DEPTH_BIT);
}

static void copyBuffer(VkBuffer src, VkBuffer dst, VkDeviceSize size)
{
    VkCommandBuffer tbuffer = beginSingleCommand();
    VkBufferCopy copy {};
    copy.srcOffset = 0;
    copy.dstOffset = 0;
    copy.size = size;
    vkCmdCopyBuffer(tbuffer,src,dst,1,&copy);
    endSingleCommand(tbuffer);
}

static VkCommandBuffer beginSingleCommand()
{
    VkCommandBufferAllocateInfo info {};
    info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    info.commandPool = transientCPool;
    info.commandBufferCount = 1;
    VkCommandBuffer tbuffer;
    vkAllocateCommandBuffers(logicalDevice,&info,&tbuffer);

    VkCommandBufferBeginInfo begin {};
    begin.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    begin.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    vkBeginCommandBuffer(tbuffer,&begin);
    return tbuffer;
}

static void endSingleCommand(VkCommandBuffer buffer)
{
    vkEndCommandBuffer(buffer);
    VkSubmitInfo submit {};
    submit.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submit.commandBufferCount = 1;
    submit.pCommandBuffers = &buffer;
    vkQueueSubmit(graphicsQueue,1,&submit,VK_NULL_HANDLE);
    vkQueueWaitIdle(graphicsQueue);

    vkFreeCommandBuffers(logicalDevice,transientCPool,1,&buffer);
}

static void createIndexBuffer()
{
    VkDeviceSize bufferSize = sizeof(uint16_t) * indices.count;
    VkBuffer stagingBuffer;
    VmaAllocation stagingMemory;
    createBuffer(bufferSize,VK_BUFFER_USAGE_TRANSFER_SRC_BIT,VMA_ALLOCATION_CREATE_HOST_ACCESS_RANDOM_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, &stagingBuffer,&stagingMemory);

    void* data;
    vmaMapMemory(allocator,stagingMemory,&data);
    memcpy(data,indices.data,bufferSize);
    vmaUnmapMemory(allocator,stagingMemory);

    createBuffer(bufferSize,VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT,VMA_ALLOCATION_CREATE_STRATEGY_BEST_FIT_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,&indexBuffer,&indexMemory);
    copyBuffer(stagingBuffer,indexBuffer,bufferSize);
    vmaDestroyBuffer(allocator,stagingBuffer,stagingMemory);
    // vkDestroyBuffer(logicalDevice,stagingBuffer,NULL);
    // vkFreeMemory(logicalDevice,stagingMemory,NULL);
}

static void createVertexBuffer()
{

    
    VkBuffer stagingBuffer;
    VmaAllocation stagingMemory;
    printf("attempting allocation of %i bytes for vert buffer\n",sizeof(Vertex3_t) * verts.count);
    createBuffer(sizeof(Vertex3_t) * verts.count,VK_BUFFER_USAGE_TRANSFER_SRC_BIT,VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT,VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,&stagingBuffer,&stagingMemory);
    void *data;
    if(!stagingMemory) {
        printf("staging memory null\n");
    }
    if(!allocator)
    {
        printf("allocator null\n");
    }
    vmaMapMemory(allocator,stagingMemory,&data);
    memcpy(data,verts.data,sizeof(Vertex3_t) * verts.count);
    vmaUnmapMemory(allocator,stagingMemory);

    createBuffer(sizeof(Vertex3_t) * verts.count,VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,VMA_ALLOCATION_CREATE_STRATEGY_BEST_FIT_BIT,VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,&vertexBuffer,&vBufferMemory);
    
    copyBuffer(stagingBuffer,vertexBuffer,sizeof(Vertex3_t) * verts.count);
    vmaDestroyBuffer(allocator,stagingBuffer,stagingMemory);
    // vkDestroyBuffer(logicalDevice,stagingBuffer,NULL);
    // vkFreeMemory(logicalDevice,stagingMemory,NULL);
}

static void createSyncObjects()
{
    VkSemaphoreCreateInfo si {};
    si.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

    VkFenceCreateInfo fi {};
    fi.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fi.flags = VK_FENCE_CREATE_SIGNALED_BIT;
    for(int i = 0; i < MAX_FRAMES_IN_FLIGHT; i++)
    if(vkCreateSemaphore(logicalDevice,&si,NULL,&imageAvailableSem[i]) != VK_SUCCESS ||
       vkCreateSemaphore(logicalDevice,&si,NULL,&renderFinishedSem[i]) != VK_SUCCESS ||
       vkCreateFence(logicalDevice,&fi,NULL,&inFlightFence[i]) != VK_SUCCESS)
       {
           printf("failed to create synchronization structures\n");
           assert(0);
       }
}

static void createDescriptorPool()
{
    VkDescriptorPoolSize poolSize {};
    poolSize.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    poolSize.descriptorCount = MAX_FRAMES_IN_FLIGHT;

    VkDescriptorPoolSize imagePoolSize {};
    imagePoolSize.type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    imagePoolSize.descriptorCount = MAX_FRAMES_IN_FLIGHT;

    VkDescriptorPoolSize sizes[2] = {poolSize, imagePoolSize};
    
    VkDescriptorPoolCreateInfo info {};
    info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    info.poolSizeCount = 2;
    info.pPoolSizes = sizes;
    info.maxSets = MAX_FRAMES_IN_FLIGHT;

    if(vkCreateDescriptorPool(logicalDevice, &info, NULL, &descriptorPool) != VK_SUCCESS)
    {
        printf("failed to create descriptor pool\n");
        assert(0);
    }

}

static void createDescriptorSets()
{
    VkDescriptorSetLayout layouts[MAX_FRAMES_IN_FLIGHT] = {
        setLayout,
        setLayout
    };
    VkDescriptorSetAllocateInfo info {};
    info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    info.descriptorPool = descriptorPool;
    info.descriptorSetCount = MAX_FRAMES_IN_FLIGHT;
    info.pSetLayouts = layouts;
    VkResult res = vkAllocateDescriptorSets(logicalDevice,&info,descriptorSets);
    if(res != VK_SUCCESS)
    {
        printf("unable to create descriptor sets, error code: %i\n",res);
        assert(0);
    }

    for(int i = 0; i < MAX_FRAMES_IN_FLIGHT; i++)
    {
        VkDescriptorBufferInfo bInfo {};
        bInfo.buffer = uniformBuffers[i];
        bInfo.offset = 0;
        bInfo.range = sizeof(UniformBufferObject_t);

        VkDescriptorImageInfo iInfo{};
        iInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        iInfo.imageView = dashView;
        iInfo.sampler = dashSampler;
        
        VkWriteDescriptorSet write {};
        write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        write.dstSet = descriptorSets[i];
        write.dstBinding = 0;
        write.dstArrayElement = 0;
        write.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        write.descriptorCount = 1;
        write.pBufferInfo = &bInfo;
        write.pImageInfo = NULL;
        write.pTexelBufferView = NULL;
        
        VkWriteDescriptorSet dashwrite {};
        dashwrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        dashwrite.dstSet = descriptorSets[i];
        dashwrite.dstBinding = 1;
        dashwrite.dstArrayElement = 0;
        dashwrite.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        dashwrite.descriptorCount = 1;
        dashwrite.pImageInfo = &iInfo;
        dashwrite.pTexelBufferView = NULL;


        VkWriteDescriptorSet writes[2] = {write,dashwrite};

        vkUpdateDescriptorSets(logicalDevice,2,writes,0,NULL);
    }

}

static void recreateSwapChain()
{
    vkDeviceWaitIdle(logicalDevice);
    cleanupSwapChain();
    createSwapChain();
    createImageViews();
    createRenderPass();
    createGraphicsPipeline();
    createFrameBuffers();
    // createUniformBuffers();
}
float delta = 0.0F;
static void updateUniformBuffer(uint32_t currentImage)
{
    timespec time;
    clock_gettime(CLOCK_MONOTONIC,&time);
    delta = time.tv_sec - prevTime.tv_sec;
    delta += time.tv_nsec / 1000000000.0f;
    // if(time.tv_nsec < prevTime.tv_nsec) delta += 1;
    // prevTime = time;

    UniformBufferObject_t ubo {};
    // printf("delta %f\n",delta);

    //1 0 0 0 
    //0 1 0 0 
    //0 0 1 0 
    //0 0 0 1 

    ubo.model = glm::rotate(glm::rotate(glm::mat4(1.0f),glm::radians(90.0f),glm::vec3(1.0f,0.0f,0.0f)),(delta) * glm::radians(90.0f), glm::vec3(0.0f,1.0f,0.0f));
    ubo.view = glm::lookAt(glm::vec3(2.0f,2.0f,4.0f), glm::vec3(0.0f,0.0f,0.0f), glm::vec3(0.0f,0.0f,1.0f));

    ubo.proj = glm::perspective(glm::radians(45.0f), scExtent.width /(float) scExtent.height, 0.1f,10.0f);
    ubo.proj[1][1] *= -1;

    void* data;
    vmaMapMemory(allocator,uBuffersMemory[currentImage],&data);
    memcpy(data, &ubo, sizeof(ubo));
    vmaUnmapMemory(allocator,uBuffersMemory[currentImage]);

}

void drawFrame()
{
    // printf("drawing frame\n");
    vkWaitForFences(logicalDevice,1,&inFlightFence[currentFrame],VK_TRUE,UINT64_MAX);
    
    uint32_t imageIndex;
    VkResult res = vkAcquireNextImageKHR(logicalDevice,swapchain,UINT64_MAX,imageAvailableSem[currentFrame],VK_NULL_HANDLE,&imageIndex);
    if(res == VK_ERROR_OUT_OF_DATE_KHR)
    {
        recreateSwapChain();
        return;
    }else if(res != VK_SUCCESS && res != VK_SUBOPTIMAL_KHR)
    {
        printf("could not get next swapchain image\n");
        assert(0);
    }
    updateUniformBuffer(currentFrame);
    vkResetFences(logicalDevice,1,&inFlightFence[currentFrame]);
    vkResetCommandBuffer(commandBuffer[currentFrame],0);
    recordCommandBuffer(commandBuffer[currentFrame],imageIndex);
    
    VkSubmitInfo si {};
    si.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    VkSemaphore waitSems[] = {imageAvailableSem[currentFrame]};
    VkPipelineStageFlags waitStages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
    si.waitSemaphoreCount = 1;
    si.pWaitSemaphores = waitSems;
    si.pWaitDstStageMask = waitStages;
    si.commandBufferCount = 1;
    si.pCommandBuffers = &commandBuffer[currentFrame];
    VkSemaphore signalSems[] = {renderFinishedSem[currentFrame]};
    si.signalSemaphoreCount = 1;
    si.pSignalSemaphores = signalSems;

    if(vkQueueSubmit(graphicsQueue,1,&si,inFlightFence[currentFrame]) != VK_SUCCESS)
    {
        printf("failed to submit to queue\n");
        assert(0);
    }

    VkPresentInfoKHR pi {};
    pi.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    pi.waitSemaphoreCount = 1;
    pi.pWaitSemaphores = signalSems;
    VkSwapchainKHR swapChains[] = {swapchain};
    pi.swapchainCount = 1;
    pi.pSwapchains = swapChains;
    pi.pImageIndices = &imageIndex;
    pi.pResults = NULL;
    res = vkQueuePresentKHR(presentQueue,&pi);
    if(res == VK_ERROR_OUT_OF_DATE_KHR || res == VK_SUBOPTIMAL_KHR || resize)
    {
        recreateSwapChain();
        resize = false;
    }else if(res != VK_SUCCESS)
    {
        printf("could present swapchain image\n");
        assert(0);
    }

    currentFrame = (currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
}

static void setupValidationLayers()
{
    //set up some validation for the validation layers
}

int pollEvent()
{
    SDL_Event event;
    if(SDL_PollEvent(&event))
    {
        if(event.type == SDL_QUIT)
        {
            shutDown(NULL,&event);
            return 1;
        }
        if( event.type == SDL_WINDOWEVENT)
        {
            // printf("window event detected %i\n",event.window.event);
            if(event.window.event ==  SDL_WINDOWEVENT_SIZE_CHANGED)
            {
                // printf("resize detected\n");
                resize = true;
                return 0;
            }
        }
    }
    return 0;
}

static void cleanupSwapChain()
{
    for(int i = 0; i < scImageCount;i++)
    {
        vkDestroyFramebuffer(logicalDevice,framebuffers[i],NULL);
    }
    vkDestroyPipeline(logicalDevice,graphicsPipeline,NULL);
    vkDestroyPipelineLayout(logicalDevice,pipelineLayout,NULL);
    vkDestroyRenderPass(logicalDevice,renderPass,NULL);
    for(int i = 0; i < scImageCount;i++)
    {
        vkDestroyImageView(logicalDevice,scImageViews[i],NULL);
    }
    vkDestroySwapchainKHR(logicalDevice,swapchain,NULL);
    
}

int shutDown(void *userdata,SDL_Event *event)
{
    switch(event->type)
    {
        case SDL_QUIT:
        {
            #ifdef VULKAN_TEST
            vkDeviceWaitIdle(logicalDevice);
            for(int i = 0; i < MAX_FRAMES_IN_FLIGHT; i++)
            {
                vkDestroySemaphore(logicalDevice,imageAvailableSem[i],NULL);
                vkDestroySemaphore(logicalDevice, renderFinishedSem[i], NULL);
                vkDestroyFence(logicalDevice,inFlightFence[i],NULL);
            }
            for(int i = 0; i < MAX_FRAMES_IN_FLIGHT; i++)
            {
                vmaDestroyBuffer(allocator,uniformBuffers[i],uBuffersMemory[i]);
                // vkDestroyBuffer(logicalDevice,uniformBuffers[i],NULL);
                // vkFreeMemory(logicalDevice,uBuffersMemory[i],NULL);
            }
            free(verts.data);
            free(indices.data);
            printf("destroy1\n");
            vkDestroyImageView(logicalDevice,depthView,NULL);
            printf("destroy2\n");
            vmaDestroyImage(allocator,depthImage,NULL);
            printf("destroy3\n");
            vkDestroySampler(logicalDevice,dashSampler,NULL);
            printf("destroy4\n");

            vkDestroyImageView(logicalDevice,dashView,NULL);
            printf("destroy5\n");
            vmaDestroyImage(allocator,dash,NULL);
            printf("destroy6\n");
            vkDestroyDescriptorPool(logicalDevice,descriptorPool,NULL);
            printf("destroy7\n");
            vkDestroyCommandPool(logicalDevice,commandPool,NULL);
            printf("destroy8\n");
            vkDestroyCommandPool(logicalDevice,transientCPool,NULL);
            cleanupSwapChain();
            vkDestroyDescriptorSetLayout(logicalDevice,setLayout,NULL);
            vmaDestroyBuffer(allocator,indexBuffer,indexMemory);
            // vkDestroyBuffer(logicalDevice,indexBuffer,NULL);
            // vkFreeMemory(logicalDevice,indexMemory,NULL);
            vmaDestroyBuffer(allocator,vertexBuffer,vBufferMemory);
            // vkDestroyBuffer(logicalDevice,vertexBuffer,NULL);
            // vkFreeMemory(logicalDevice,vBufferMemory,NULL);
            printf("destroy alloc\n");
            vmaDestroyAllocator(allocator);
            vkDestroySurfaceKHR(instance,vulkan_surface,NULL);
            vkDestroyDevice(logicalDevice,NULL);
            vkDestroyInstance(instance,NULL);
            #endif
            return 1;
            // Shutdown();

            // exit(0);
        }
    }
    return 0;
}

void clearScreen()
{
    SDL_SetRenderDrawColor(renderer,0,0,0,0);
    SDL_RenderClear(renderer);
}

void drawBox(uint8_t r, uint8_t g, uint8_t b, uint32_t xStart, uint32_t yStart, uint32_t size)
{
    SDL_SetRenderDrawColor(renderer,r,g,b,0);
    SDL_Rect rect;
    rect.h = size;
    rect.w = size;
    rect.x = xStart;
    rect.y = yStart;
    if(SDL_RenderFillRect(renderer,&rect) == -1)
    {
        printf(SDL_GetError());
        assert(0);
    }
    
}

const SDL_Color white = {0xFF,0xFF,0xFF};
void drawText(const char *text,uint32_t xStart,uint32_t yStart,uint32_t xSize,uint32_t ySize)
{
    
    SDL_Surface *textSurface = TTF_RenderText_Solid(messageFont,text,white);
    SDL_Texture *textTexture = SDL_CreateTextureFromSurface(renderer,textSurface);
    SDL_Rect textBox;
    textBox.w = xSize;
    textBox.h = ySize;
    textBox.y = yStart;
    textBox.x = xStart;

    SDL_RenderCopy(renderer,textTexture,NULL,&textBox);
    SDL_DestroyTexture(textTexture);
    SDL_FreeSurface(textSurface);

}

void updateWindow()
{  
    SDL_RenderPresent(renderer);
}